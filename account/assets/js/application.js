// ***********************************************************
//
// Extension
// - 利用しているライブラリの拡張など
//
// ***********************************************************
// ***********************************************************
// jquery.cookieの拡張
// expireなどのoptionをアプリ独自設定に変更
// 一旦
// ***********************************************************
$.extend({
	app_cookie : function(key, value, options) {
		if (arguments.length == 1) return $.cookie(key);

		options = options || {};
		if (undefined == options['expires'] || null == options['expires']) {
 			// expireは１時間後を設定
// 			var date = new Date();
// 			date.setTime( date.getTime() + (60 * 60 * 1000));
			options['expires'] = '';
		}
		$.cookie(key, value, options);

		var keys = $.cookie('keys');
		keys = (undefined != keys || null != keys) ? JSON.parse(keys) : [];
		if (keys.indexOf(key) == -1) keys.push(key);

		$.cookie('keys', JSON.stringify(keys));
	},
	app_cookie_remove_all : function()
	{
		var keys = $.cookie('keys');
		if (undefined != keys || null != keys) {
			keys = JSON.parse(keys);
			$.each(keys, function(e, v){
				$.removeCookie(v);
			});
		}
		$.removeCookie('keys');
	}
});


// ***********************************************************
// 共通利用出来るバリデーションを定義
// ***********************************************************
if ($.validator) {
	$.validator.setDefaults({
		ignore: ".ignore,:hidden"
	});
	/**
	 * 必須チェック visibleオプションあり
	 */
	$.validator.addMethod("optional_required", function(value, element) {
		// data-enabled="true"の場合、enabledに設定されていない時だけチェック対象
		// enabledの場合、ノーチェック。即ちtrueを返却
		var enabled = $(element).attr('data-enabled');
		if (enabled && 'true' == enabled && $(element).is(':disabled')) return true;

		// data-visible="true"の場合、表示されている時だけチェック対象
		// 非表示の場合、ノーチェック。即ちtrueを返却
		var visible = $(element).attr('data-visible');
		if (visible && 'true' == visible && $(element).is(':hidden')) return true;

		var type = $(element).attr('type');
		if ('radio' == type || 'checkbox' == type) {
			return $('input[name="' + element.name + '"]:checked').length > 0;
		}
		return undefined != value && null != value && value.length > 0;
	}, "");

	/**
	 * 複数チェックボックスで最低１つチェックが必要な場合のバリデーション
	 */
	$.validator.addMethod("one_or_more_required", function(value, element) {
		// data-visible="true"の場合、表示されている時だけチェック対象
		// 非表示の場合、ノーチェック。即ちtrueを返却
		var visible = $(element).attr('data-visible');
		if (visible && 'true' == visible && $(element).is(':hidden')) return true;

		var result = false;
		// その他テキストが存在する場合のオプション
		var others = $(element).attr('data-others');
		if (others && others.length > 0) {
			result = $(others).get()
				.map(function(e){ return $(e).val(); })
				.filter(function(e) { return e && e.length > 0 })
				.length > 0;
		}
		result |= $('input[name="' + element.name + '"]:checked').length > 0;

		return result;
	}, "");

	/**
	 * 指定したグループが必須要件を満たすかどうかのチェック処理
	 */
	$.validator.addMethod("optional_required_from_group", function(value, element) {
		var $element = $(element);

		// data-visible="true"の場合、表示されている時だけチェック対象
		// 非表示の場合、ノーチェック。即ちtrueを返却
		var visible = $(element).attr('data-visible');
		if (visible && 'true' == visible && $(element).is(':hidden')) return true;

		var group = $($element.attr('data-group'));
		var requiredCount = $element.attr('data-required-count');

		var $elements = group.get().map(function(e) { return $(e); }).filter(function(e) { return e.is(':visible'); });
		$elements.push($element);

		var result =  $elements.filter(function(e) {
			if ('checkbox' == e.attr('type')) {
				return e.prop('checked');
			} else {
				return undefined !== e.val() && null != e.val() && e.val().length > 0;
			}
		}).length >= requiredCount;

		return result;
	}, "");


	// ***********************************************************
	// アプリ固有のバリデーションを定義
	// ***********************************************************
	/**
	 * アプリ独自の電話グループ毎のバリデーション
	 */
	$.validator.addMethod("app_group_phone", function(value, element) {
		var $element = $(element);
		var group = $($element.attr('data-group'));
		var otherGroup = $($element.attr('data-other-group'));
		var groupCount = parseInt($element.attr('data-group-count'));

		var values = group.get().map(function(e) { return $(e).val() }).filter(function(e) { return undefined !== e && null != e && e.length > 0 });
		var otherValues = otherGroup.get().map(function(e) { return $(e).val() }).filter(function(e) { return undefined !== e && null != e && e.length > 0 });

		return groupCount === values.length || groupCount == otherValues.length;
	}, "");
}


// ***********************************************************
// アプリケーションの独自
// ***********************************************************
/**
 * アプリケーション固有のJSを定義
 * @type {{}|*}
 */
window.App = window.App || {};

/**
 * 定数定義
 * @type {{}|*|window.App.const}
 */
window.App.const = window.App.const || {};
// 職種
App.const.OCCUPATION = [
	{ "label": '民間企業（未上場）の社員・団体職員',		"value": 0,		"data-callback": 'selectedOccupation1',	"data-show-insider-warning": false },
	{ "label": '民間企業（未上場）の管理職・団体管理職',	"value": 1,		"data-callback": 'selectedOccupation2',	"data-show-insider-warning": false },
	{ "label": '民間企業（未上場）の役員・団体役員',		"value": 2,		"data-callback": 'selectedOccupation1',	"data-show-insider-warning": false },
	{ "label": '民間企業（上場）の社員',				"value": 3,		"data-callback": 'selectedOccupation3',	"data-show-insider-warning": true },
	{ "label": '民間企業（上場）の管理職',				"value": 4,		"data-callback": 'selectedOccupation4',	"data-show-insider-warning": true },
	{ "label": '民間企業（上場）の役員',				"value": 5,		"data-callback": 'selectedOccupation5',	"data-show-insider-warning": true },
	{ "label": '官公庁職員',							"value": 6,		"data-callback": 'selectedOccupation1',	"data-show-insider-warning": false },
	{ "label": '官公庁管理職',						"value": 7,		"data-callback": 'selectedOccupation2',	"data-show-insider-warning": false },
	{ "label": '弁護士',								"value": 9,		"data-callback": 'selectedOccupation6',	"data-show-insider-warning": false },
	{ "label": '税理士・公認会計士',					"value": 10,	"data-callback": 'selectedOccupation6',	"data-show-insider-warning": false },
	{ "label": '教職員',								"value": 12,	"data-callback": 'selectedOccupation7',	"data-show-insider-warning": false },
	{ "label": '自営業',								"value": 11,	"data-callback": 'selectedOccupation8',	"data-show-insider-warning": false },
	{ "label": '医師（開業）',						"value": 13,	"data-callback": 'selectedOccupation7',	"data-show-insider-warning": false },
	{ "label": '医師（勤務）',						"value": 14,	"data-callback": 'selectedOccupation7',	"data-show-insider-warning": false },
	{ "label": 'パート・アルバイト・派遣',				"value": 15,	"data-callback": 'selectedOccupation6',	"data-show-insider-warning": false },
	{ "label": '専業主婦',							"value": 16,	"data-callback": 'selectedOccupation9',	"data-show-insider-warning": false },
	{ "label": '学生',								"value": 17,	"data-callback": 'selectedOccupation9',	"data-show-insider-warning": false },
	{ "label": '無職',								"value": 18,	"data-callback": 'selectedOccupation9',	"data-show-insider-warning": false }
];

// 役職
App.const.POSITION = [
	{ "label": '部長職',		"value": 1 },
	{ "label": '次長職',		"value": 2 },
	{ "label": '課長職',		"value": 3 },
	{ "label": '支店長職',	"value": 4 },
	{ "label": '所長職',		"value": 5 },
	{ "label": '理事職',		"value": 6 },
	{ "label": '参事職',		"value": 7 },
	{ "label": '顧問職',		"value": 8 },
	{ "label": 'リーダー職',	"value": 9 },
	{ "label": '執行役員',	"value": 10 },
	{ "label": 'その他',		"value": 0 }
];

// 業種
App.const.INDUSTRY = [
	{ "label": '水産・農林業',		"value": 1 },
	{ "label": '建設・製造業',		"value": 2 },
	{ "label": '運輸・情報通信業',		"value": 3 },
	{ "label": '商業（卸売・小売）',	"value": 4 },
	{ "label": '金融・保険業',		"value": 5 },
	{ "label": '不動産業',			"value": 6 },
	{ "label": '飲食業',				"value": 7 },
	{ "label": 'サービス業',			"value": 8 },
	{ "label": 'コンサルタント業',		"value": 9 },
	{ "label": 'その他',				"value": 0 }
];

App.const.INSIDER_CATEGORY = [
	{ "label": '役員',					"value": 1 },
	{ "label": '主要株主',				"value": 2 },
	{ "label": '大株主',					"value": 3 },
	{ "label": '退任役員（退任後1年未満）',	"value": 4 },
	{ "label": '職員（管理職）',			"value": 5 },
	{ "label": '職員（一般職）',			"value": 6 }
];


// 国
App.const.COUNTRY = [
	{ "label": '中国', "value": 105},
	{ "label": '韓国', "value": 103},
	{ "label": 'アイスランド', "value": 201},
	{ "label": 'アイルランド', "value": 206},
	{ "label": 'アゼルバイジャン', "value": 150},
	{ "label": 'アゾレス', "value": 216},
	{ "label": 'アフガニスタン', "value": 130},
	{ "label": 'アメリカ合衆国', "value": 304},
	{ "label": 'アラブ首長国連邦', "value": 147},
	{ "label": 'アルジェリア', "value": 503},
	{ "label": 'アルゼンチン', "value": 413},
	{ "label": 'アルバニア', "value": 229},
	{ "label": 'アルメニア', "value": 151},
	{ "label": 'アンギラ', "value": 337},
	{ "label": 'アンゴラ', "value": 535},
	{ "label": 'アンティグア・バーブーダ', "value": 331},
	{ "label": 'アンドラ', "value": 212},
	{ "label": 'イエメン', "value": 149},
	{ "label": 'イギリス', "value": 205},
	{ "label": 'イスラエル', "value": 143},
	{ "label": 'イタリア', "value": 220},
	{ "label": 'イラク', "value": 134},
	{ "label": 'イラン', "value": 133},
	{ "label": 'インド', "value": 123},
	{ "label": 'インドネシア', "value": 118},
	{ "label": 'ウガンダ', "value": 542},
	{ "label": 'ウクライナ', "value": 238},
	{ "label": 'ウズベキスタン', "value": 152},
	{ "label": 'ウルグアイ', "value": 412},
	{ "label": 'エクアドル', "value": 406},
	{ "label": 'エジプト', "value": 506},
	{ "label": 'エストニア', "value": 235},
	{ "label": 'エチオピア', "value": 538},
	{ "label": 'エリトリア', "value": 559},
	{ "label": 'エルサルバドル', "value": 309},
	{ "label": 'オーストラリア', "value": 601},
	{ "label": 'オーストリア', "value": 225},
	{ "label": 'オマーン', "value": 141},
	{ "label": 'オランダ', "value": 207},
	{ "label": 'カーボヴェルデ', "value": 522},
	{ "label": 'カザフスタン', "value": 153},
	{ "label": 'カタール', "value": 140},
	{ "label": 'カナダ', "value": 302},
	{ "label": 'カナリー諸島', "value": 523},
	{ "label": 'カメルーン', "value": 527},
	{ "label": 'カンボジア', "value": 120},
	{ "label": 'ガーナ', "value": 517},
	{ "label": 'ガーンジィ', "value": 041},
	{ "label": 'ガイアナ', "value": 403},
	{ "label": 'ガザ', "value": 148},
	{ "label": 'ガボン', "value": 531},
	{ "label": 'ガンビア', "value": 511},
	{ "label": 'キプロス', "value": 233},
	{ "label": 'キューバ', "value": 321},
	{ "label": 'キリバス', "value": 615},
	{ "label": 'キルギス', "value": 154},
	{ "label": 'ギニア', "value": 513},
	{ "label": 'ギニアビサウ', "value": 512},
	{ "label": 'ギリシャ', "value": 230},
	{ "label": 'クウェート', "value": 138},
	{ "label": 'クック諸島', "value": 607},
	{ "label": 'クロアチア', "value": 241},
	{ "label": 'グアテマラ', "value": 306},
	{ "label": 'グアム', "value": 620},
	{ "label": 'グリーンランド', "value": 301},
	{ "label": 'グルジア', "value": 157},
	{ "label": 'グレナダ', "value": 329},
	{ "label": 'ケイマン諸島', "value": 328},
	{ "label": 'ケニア', "value": 541},
	{ "label": 'コートジボワール', "value": 516},
	{ "label": 'コスタリカ', "value": 311},
	{ "label": 'コモロ', "value": 558},
	{ "label": 'コロンビア', "value": 401},
	{ "label": 'コンゴ共和国', "value": 532},
	{ "label": 'コンゴ民主共和国', "value": 533},
	{ "label": 'サウジアラビア', "value": 137},
	{ "label": 'サントメ・プリンシペ', "value": 536},
	{ "label": 'サンピエール及びミクロン島', "value": 303},
	{ "label": 'サンマリノ', "value": 298},
	{ "label": 'ザンビア', "value": 554},
	{ "label": 'シエラレオネ', "value": 514},
	{ "label": 'シリア', "value": 145},
	{ "label": 'シンガポール', "value": 112},
	{ "label": 'ジブチ', "value": 539},
	{ "label": 'ジブラルタル', "value": 219},
	{ "label": 'ジャージィー', "value": 43},
	{ "label": 'ジャマイカ', "value": 316},
	{ "label": 'ジンバブエ', "value": 549},
	{ "label": 'スーダン', "value": 507},
	{ "label": 'スイス', "value": 215},
	{ "label": 'スウェーデン', "value": 203},
	{ "label": 'スペイン', "value": 218},
	{ "label": 'スリナム', "value": 404},
	{ "label": 'スリランカ', "value": 125},
	{ "label": 'スロバキア', "value": 246},
	{ "label": 'スロベニア', "value": 242},
	{ "label": 'スワジランド', "value": 556},
	{ "label": 'セーシェル', "value": 544},
	{ "label": 'セウタ及びメリリヤ', "value": 502},
	{ "label": 'セネガル', "value": 510},
	{ "label": 'セルビア・モンテネグロ', "value": 228},
	{ "label": 'セントクリストファー・ネーヴィス', "value": 335},
	{ "label": 'セントビンセント', "value": 336},
	{ "label": 'セントヘレナ', "value": 537},
	{ "label": 'セントルシア', "value": 330},
	{ "label": 'ソマリア', "value": 540},
	{ "label": 'ソロモン', "value": 613},
	{ "label": 'タークス及びカイコス諸島', "value": 317},
	{ "label": 'タイ', "value": 111},
	{ "label": 'タジキスタン', "value": 155},
	{ "label": 'タンザニア', "value": 543},
	{ "label": 'チェコ', "value": 245},
	{ "label": 'チャド', "value": 528},
	{ "label": 'チュニジア', "value": 504},
	{ "label": 'チリ', "value": 409},
	{ "label": 'ツバル', "value": 624},
	{ "label": 'デンマーク', "value": 204},
	{ "label": 'トーゴ', "value": 518},
	{ "label": 'トケラウ諸島', "value": 608},
	{ "label": 'トリニダード・トバゴ', "value": 320},
	{ "label": 'トルクメニスタン', "value": 156},
	{ "label": 'トルコ', "value": 234},
	{ "label": 'トンガ', "value": 614},
	{ "label": 'ドイツ', "value": 213},
	{ "label": 'ドミニカ', "value": 333},
	{ "label": 'ドミニカ共和国', "value": 323},
	{ "label": 'ナイジェリア', "value": 524},
	{ "label": 'ナウル', "value": 617},
	{ "label": 'ナミビア', "value": 550},
	{ "label": 'ニウェ島', "value": 609},
	{ "label": 'ニカラグア', "value": 310},
	{ "label": 'ニジェール', "value": 525},
	{ "label": 'ニューカレドニア', "value": 618},
	{ "label": 'ニュージーランド', "value": 606},
	{ "label": 'ネパール', "value": 131},
	{ "label": 'ノルウェー', "value": 202},
	{ "label": 'ハイチ', "value": 322},
	{ "label": 'ハンガリー', "value": 227},
	{ "label": 'バージン諸島', "value": 325},
	{ "label": 'バーレーン', "value": 135},
	{ "label": 'バチカン', "value": 299},
	{ "label": 'バヌアツ', "value": 611},
	{ "label": 'バハマ', "value": 315},
	{ "label": 'バミューダ諸島', "value": 314},
	{ "label": 'バルバドス', "value": 319},
	{ "label": 'バングラデシュ', "value": 127},
	{ "label": 'パキスタン', "value": 124},
	{ "label": 'パナマ', "value": 312},
	{ "label": 'パプアニューギニア', "value": 602},
	{ "label": 'パラオ', "value": 628},
	{ "label": 'パラグアイ', "value": 411},
	{ "label": 'ピットケルン', "value": 616},
	{ "label": 'フィジー', "value": 612},
	{ "label": 'フィリピン', "value": 117},
	{ "label": 'フィンランド', "value": 222},
	{ "label": 'フォークランド諸島', "value": 414},
	{ "label": 'フランス', "value": 210},
	{ "label": 'ブータン', "value": 132},
	{ "label": 'ブラジル', "value": 410},
	{ "label": 'ブルガリア', "value": 232},
	{ "label": 'ブルキナファソ', "value": 521},
	{ "label": 'ブルネイ', "value": 116},
	{ "label": 'ブルンジ', "value": 534},
	{ "label": 'プエルトリコ', "value": 324},
	{ "label": 'ベトナム', "value": 110},
	{ "label": 'ベナン', "value": 519},
	{ "label": 'ベネズエラ', "value": 402},
	{ "label": 'ベラルーシ', "value": 239},
	{ "label": 'ベリーズ', "value": 308},
	{ "label": 'ベルギー', "value": 208},
	{ "label": 'ペルー', "value": 407},
	{ "label": 'ホンジュラス', "value": 307},
	{ "label": 'ボスニア・ヘルツェゴビナ', "value": 243},
	{ "label": 'ボツワナ', "value": 555},
	{ "label": 'ボリビア', "value": 408},
	{ "label": 'ポーランド', "value": 223},
	{ "label": 'ポルトガル', "value": 217},
	{ "label": 'マーシャル', "value": 625},
	{ "label": 'マカオ', "value": 129},
	{ "label": 'マケドニア 旧ユーゴスラビア共和国', "value": 244},
	{ "label": 'マダガスカル', "value": 546},
	{ "label": 'マラウイ', "value": 553},
	{ "label": 'マリ', "value": 520},
	{ "label": 'マルタ', "value": 221},
	{ "label": 'マレーシア', "value": 113},
	{ "label": 'マン島', "value": 60},
	{ "label": 'ミクロネシア', "value": 626},
	{ "label": 'ミャンマー', "value": 122},
	{ "label": 'メキシコ', "value": 305},
	{ "label": 'モーリシャス', "value": 547},
	{ "label": 'モーリタニア', "value": 509},
	{ "label": 'モザンビーク', "value": 545},
	{ "label": 'モナコ', "value": 211},
	{ "label": 'モルディブ', "value": 126},
	{ "label": 'モルドバ', "value": 240},
	{ "label": 'モロッコ', "value": 501},
	{ "label": 'モンゴル', "value": 107},
	{ "label": 'モントセラト', "value": 334},
	{ "label": 'ヨルダン', "value": 144},
	{ "label": 'ラオス', "value": 121},
	{ "label": 'ラトビア', "value": 236},
	{ "label": 'リトアニア', "value": 237},
	{ "label": 'リヒテンシュタイン', "value": 297},
	{ "label": 'リビア', "value": 505},
	{ "label": 'リベリア', "value": 515},
	{ "label": 'ルーマニア', "value": 231},
	{ "label": 'ルクセンブルク', "value": 209},
	{ "label": 'ルワンダ', "value": 526},
	{ "label": 'レソト', "value": 552},
	{ "label": 'レバノン', "value": 146},
	{ "label": 'レユニオン', "value": 548},
	{ "label": 'ロシア', "value": 224},
	{ "label": 'ワリス・フテュナ諸島', "value": 697},
	{ "label": '英領ヴァージン諸島', "value": 332},
	{ "label": '香港', "value": 108},
	{ "label": '西サハラ', "value": 508},
	{ "label": '西サモア', "value": 610},
	{ "label": '赤道ギニア', "value": 530},
	{ "label": '台湾', "value": 106},
	{ "label": '中央アフリカ', "value": 529},
	{ "label": '朝鮮民主主義人民共和国', "value": 104},
	{ "label": '東ティモール', "value": 128},
	{ "label": '南アフリカ共和国', "value": 551},
	{ "label": '米領オセアニア', "value": 622},
	{ "label": '米領サモア', "value": 621},
	{ "label": '北マリアナ諸島', "value": 627},
	{ "label": '蘭領アンティル', "value": 326},
	{ "label": 'その他', "value": 999}
];

// 国2
App.const.COUNTRY2 = [
	{ "label": 'アイスランド共和国', "value": 0},
	{ "label": 'アイルランド', "value": 1},
	{ "label": 'アメリカ合衆国（米国）', "value": 2},
	{ "label": 'アラブ首長国連邦', "value": 3},
	{ "label": 'アルゼンチン共和国', "value": 4},
	{ "label": 'アルバ', "value": 5},
	{ "label": 'アルバニア共和国', "value": 6},
	{ "label": 'アンギラ', "value": 7},
	{ "label": 'アンティグア・バーブーダ', "value": 8},
	{ "label": 'アンドラ公国', "value": 9},
	{ "label": 'イギリス領バージン諸島', "value": 10},
	{ "label": 'イスラエル国', "value": 11},
	{ "label": 'イタリア共和国', "value": 12},
	{ "label": 'インド', "value": 13},
	{ "label": 'インドネシア共和国', "value": 14},
	{ "label": 'ウルグアイ東方共和国', "value": 15},
	{ "label": 'エストニア共和国', "value": 16},
	{ "label": 'オーストラリア連邦', "value": 17},
	{ "label": 'オーストリア共和国', "value": 18},
	{ "label": 'オランダ王国', "value": 19},
	{ "label": 'ガーナ共和国', "value": 20},
	{ "label": 'ガーンジー島', "value": 21},
	{ "label": 'カタール国', "value": 22},
	{ "label": 'カナダ', "value": 23},
	{ "label": 'キプロス共和国', "value": 24},
	{ "label": 'キュラソー島', "value": 25},
	{ "label": 'ギリシャ共和国', "value": 26},
	{ "label": 'クウェート国', "value": 27},
	{ "label": 'クック諸島', "value": 28},
	{ "label": 'グリーンランド', "value": 29},
	{ "label": 'グレートブリテン及び北アイルランド連合王国（英国）', "value": 30},
	{ "label": 'グレナダ', "value": 31},
	{ "label": 'クロアチア共和国', "value": 32},
	{ "label": 'ケイマン諸島', "value": 33},
	{ "label": 'コスタリカ共和国', "value": 34},
	{ "label": 'コロンビア共和国', "value": 35},
	{ "label": 'サウジアラビア王国', "value": 36},
	{ "label": 'サモア独立国', "value": 37},
	{ "label": 'サンマリノ共和国', "value": 38},
	{ "label": 'ジブラルタル', "value": 39},
	{ "label": 'ジャージー島', "value": 40},
	{ "label": 'シンガポール共和国', "value": 41},
	{ "label": 'スイス連邦', "value": 42},
	{ "label": 'スウェーデン王国', "value": 43},
	{ "label": 'スペイン', "value": 44},
	{ "label": 'スロバキア共和国', "value": 45},
	{ "label": 'スロベニア共和国', "value": 46},
	{ "label": 'セーシェル共和国', "value": 47},
	{ "label": 'セント・マーチン島', "value": 48},
	{ "label": 'セントクリストファー・ネーヴィス', "value": 49},
	{ "label": 'セントビンセント及びグレナディーン諸島', "value": 50},
	{ "label": 'セントルシア', "value": 51},
	{ "label": 'タークス・カイコス諸島', "value": 52},
	{ "label": 'チェコ共和国', "value": 53},
	{ "label": 'チリ共和国', "value": 54},
	{ "label": 'デンマーク王国', "value": 55},
	{ "label": 'ドイツ連邦共和国', "value": 56},
	{ "label": 'ドミニカ国', "value": 57},
	{ "label": 'トリニダード・トバゴ共和国', "value": 58},
	{ "label": 'トルコ共和国', "value": 59},
	{ "label": 'ナウル共和国', "value": 60},
	{ "label": 'ニウエ', "value": 61},
	{ "label": 'ニュージーランド', "value": 62},
	{ "label": 'ノルウェー王国', "value": 63},
	{ "label": 'バーレーン王国', "value": 64},
	{ "label": 'パナマ共和国', "value": 65},
	{ "label": 'バヌアツ共和国', "value": 66},
	{ "label": 'バハマ国', "value": 67},
	{ "label": 'バミューダ諸島', "value": 68},
	{ "label": 'バルバドス', "value": 69},
	{ "label": 'ハンガリー', "value": 70},
	{ "label": 'フィンランド共和国', "value": 71},
	{ "label": 'フェロー諸島', "value": 72},
	{ "label": 'ブラジル連邦共和国', "value": 73},
	{ "label": 'フランス共和国', "value": 74},
	{ "label": 'ブルガリア共和国', "value": 75},
	{ "label": 'ブルネイ・ダルサラーム国', "value": 76},
	{ "label": 'ベリーズ', "value": 77},
	{ "label": 'ベルギー王国', "value": 78},
	{ "label": 'ポーランド共和国', "value": 79},
	{ "label": 'ポルトガル共和国', "value": 80},
	{ "label": 'マーシャル諸島共和国', "value": 81},
	{ "label": 'マカオ', "value": 82},
	{ "label": 'マルタ共和国', "value": 83},
	{ "label": 'マレーシア', "value": 84},
	{ "label": 'マン島', "value": 85},
	{ "label": 'メキシコ合衆国', "value": 86},
	{ "label": 'モーリシャス共和国', "value": 87},
	{ "label": 'モナコ公国', "value": 88},
	{ "label": 'モントセラト', "value": 89},
	{ "label": 'ラトビア共和国', "value": 90},
	{ "label": 'リトアニア共和国', "value": 91},
	{ "label": 'リヒテンシュタイン公国', "value": 92},
	{ "label": 'ルーマニア', "value": 93},
	{ "label": 'ルクセンブルク大公国', "value": 94},
	{ "label": 'レバノン共和国', "value": 95},
	{ "label": 'ロシア連邦', "value": 96},
	{ "label": '香港', "value": 97},
	{ "label": '大韓民国', "value": 98},
	{ "label": '中華人民共和国', "value": 99},
	{ "label": '南アフリカ共和国', "value": 100},
	{ "label": 'その他', "value": 101}
];

/**
 * 指定された条件で定数を検索します。
 *
 * @param constName
 * @param key
 * @param value
 * @returns {*|null}
 */
App.const.search = function(constName, key, value) {
	var constArray = this[constName];
	return constArray.filter(function(e) { return e[key] == value }).shift() || null;
}


/**
 * ユーティリティ
 * @type {{}|*|window.app.const|window.app.const|*|{}}
 */
window.App.utils = window.App.utils || {};

/**
 * 個別ページ処理
 * - 各画面でOverloadして利用
 * @type {{}|*}
 */
window.App.page = window.App.page || {};
App.page.monitorSubmitButton = function(form) {
	var $button = form.find('[type="submit"]');
	var $modal = $('[data-modal="true"]:visible');
	if (form.valid() && 0 == $modal.length) {
		$button.prop('disabled', false);
		$button.removeClass('disabled');
	} else {
		$button.prop('disabled', true);
		$button.addClass('disabled');
	}
}


/**
 * Ready
 */
$(function() {
	$('form.js-form-validate').each(function() {
		var $form = $(this);
		$form.validate({
			// エラーメッセージは表示しない
			errorPlacement: function (error, element) {}
		});

		$form.on('blur change keyup', 'input, select', function() {
			App.page.monitorSubmitButton($form);
		});
	});

	$(document).on('shown:modal hidden:modal', '[data-modal="true"]', function() {
		$('form.js-form-validate').each(function() {
			var $form = $(this);
			App.page.monitorSubmitButton($form);
		});

		$('[data-modal-button="true"]').each(function() {
			var $button = $(this);
			var $modal = $('[data-modal="true"]:visible');
			if (0 == $modal.length) {
				$button.removeClass('rf-button-disabled');
			} else {
				$button.addClass('rf-button-disabled');
			}
		});
	});


});
