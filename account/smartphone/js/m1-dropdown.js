(function () {
    //// Showing and hiding dropdown menu on click of dropdown
    if ($('.js-dropdown').length !== 0) {
        $('.js-dropdown').each(function () {
            var $this = $(this);
            var $button = $this.find('button');
            var $menu = $this.find('.rf-dropdown--menu');
            var $title = $this.find('.js-dropdown-title');
            var menuItems = $menu.find('li');
            $button.click(function () {
                $this.toggleClass('rf-open');
            });
            // Change active item and update dropdown title
            menuItems.each(function () {
                var $this = $(this);
                $this.click(function () {
                    if (!$this.hasClass('rf-dropdown-selected')) {
                        $('.rf-dropdown-selected').removeClass('rf-dropdown-selected');
                        $this.addClass('rf-dropdown-selected');
                        var text = $this.text().split('&nbsp;')[0];
                        $title.text(text);
                        changeSort($(this).val());
                    }
                });
            });
        });
    }
})();