(function($) {
	var $button = $('#fundQuizButton');
	var $backdrop = $('.m1-modal-backdrop');
	var $modal = $('#fundQuiz');
	var $modalContent = $modal.find('.js-modal-content');
	var $closeButton = $modal.find('.m1-modal-s-close');
	var questions = {};
	var diagnosis = {};
	var loading = false;
	var setErrorMessage = function() {
		var $textNode = $('<p class="rf-pt-10 rf-pb-10">データの読み込みにエラーがありました。<br>しばらくしてからもう一度お試しください。</p>');
		$modalContent.text('');
		$modalContent.append($textNode);
	};
	var convertLineBreaks = function(string) {
		var formattedString = string.replace(/\\n/g, "<br>");
		return formattedString;
	};
	var detectNextAction = function(string) {
		if (string[0] === 'q') {
			loadQuestionUI(string[1]);
		} else if (string[0] === 'a') {
			loadDiagnosisUI(string[1]);
		} else {
			loadQuestionUI();
		}
	};
	var setAnswerEvents = function() {
		var $answers = $('.m1-modal-s-answers');
		var options = $('.m1-modal-s-answers-options a');
		var texts = $('.m1-modal-s-answers-text a');
		var $textArea1 = $($('.m1-modal-s-answers-text')[0]);
		var $textArea2 = $($('.m1-modal-s-answers-text')[1]);

		options.each(function() {
			var $this = $(this);
			$this.click(function(e) {
				var next = $this.data('next');
				var value = $this.data('value');
				if (!loading) {
					loading = true;
					$this.parent().addClass('active');
					$($textArea1.find('p')[value]).addClass('active');
					$($textArea2.find('p')[value]).addClass('active');
					$answers.addClass('m1-modal-s-answers-chosen');
					setTimeout(function() {
						detectNextAction(next);
					}, 1000);
				}
				e.preventDefault();
			});
		});

		texts.each(function() {
			var $this = $(this);
			var id;
			$this.click(function(e) {
				var next = $this.data('next');
				var value = $this.data('value');
				if (!loading) {
					loading = true;
					switch ($this.data('value')) {
						case 0:
							id = "#answerA";
							break;
						case 1:
							id = "#answerB";
							break;
						case 2:
							id = "#answerC";
							break;
						default:
							id = "#answerA";
							break;
					}

					$(id).parent().addClass('active');
					$($textArea1.find('p')[value]).addClass('active');
					$($textArea2.find('p')[value]).addClass('active');
					$answers.addClass('m1-modal-s-answers-chosen');
					setTimeout(function() {
						detectNextAction(next);
					}, 1000);
				} else {
					return false;
				}
				e.preventDefault();
			});
		});
	};
	var setBackButtonEvents = function() {
		var btn = $('.js-back-button');

		btn.each(function() {
			var $this = $(this);
			$this.click(function(e){
				var prev = $this.data('prev');
				loadQuestionUI(prev);
				e.preventDefault();
			});
		});
	};
	var loadQuestionUI = function(questionNumber) {
		var current;
		var question;
		var $textNode;
		var type;
		var answerOptions = '';
		var answerText = '';
		var backButtonHTML = '';
		var extraHTMLClass = '';

		loading = true;

		// Return X icon to gray if necessary
		if ($('.m1-modal-s-close').hasClass('rf-icon-ui-x-white')) {
			$('.m1-modal-s-close').removeClass('rf-icon-ui-x-white').addClass('rf-icon-ui-x-disabled');
		}

		// Delete background
		$('.m1-modal-s-content').css('background-image', 'none');

		var loadAnswerOptions = function(){
			switch (question.answers.length) {
				case 2:
					type = 'double';
					answerOptions = '<p><a href="#" id="answerA" data-value="0" data-next="'+question.answers[0].next+'">A</a></p><p><a href="#" id="answerB" data-value="1" data-next="'+question.answers[1].next+'">B</a></p>';
					break;
				case 3:
					type = 'triple';
					answerOptions = '<p><a href="#" id="answerA" data-value="0" data-next="'+question.answers[0].next+'">A</a></p><p><a href="#" id="answerB" data-value="1" data-next="'+question.answers[1].next+'">B</a></p><p><a href="#" id="answerC" data-value="2" data-next="'+question.answers[2].next+'">C</a></p>';
					break;
				default:
					type = 'double';
					answerOptions = '<p><a href="#" id="answerA" data-value="0" data-next="'+question.answers[0].next+'">A</a></p><p><a href="#" id="answerB" data-value="1" data-next="'+question.answers[1].next+'">B</a></p>';
					break;
			}
		};

		var loadAnswerText = function() {
			for (var answer in question.answers) {
				if (question.answers.hasOwnProperty(answer)) {
					var string = '<p><a href="#" data-value="'+answer+'" data-next="'+question.answers[answer].next+'">';
						string += convertLineBreaks(question.answers[answer].text);
						string += '</a></p>';
						answerText += string;
				}
			}
		};

		// UI loads differently for first question
		if (questionNumber && questionNumber !== 1) {
			var prev = 0;
			current = parseInt(questionNumber);
			switch (current) {
				case 2:
					prev = 1;
					break;
				case 3:
					prev = 1;
					break;
				case 4:
					prev = 1;
					break;
				case 5:
					prev = 4;
					break;
				default:
					prev = 1;
					break;
			}
			backButtonHTML = '<a href="#" data-prev="'+prev+'" class="m1-modal-s-button js-back-button">戻る</a>';
		} else {
			current = 1;
		}

		// Load question object from JSON data
		question = questions.questions[current - 1];

		// Load answer options
		loadAnswerOptions();

		// Load answer text
		loadAnswerText();

		// Add class if question contains extra images or HTML
		if (question.html !== "") {
			extraHTMLClass = "m1-modal-s-answers-extra";
		}

		// Write HTML for modal
		$textNode = $('<span class="m1-modal-s-title-tag">Q'+question.number+'</span><p class="m1-modal-s-title">'+convertLineBreaks(question.text)+'</p><div class="m1-modal-s-answers m1-modal-s-answers-'+type+' '+extraHTMLClass+'"><div class="m1-modal-s-answers-options">'+answerOptions+'</div><div class="m1-modal-s-answers-text">'+answerText+'</div>'+question.html+'</div>'+backButtonHTML);

		$modalContent.text('');
		$modalContent.append($textNode);

		setAnswerEvents();
		setBackButtonEvents();

		loading = false;
	};
	var loadDiagnosisUI = function(answerNumber) {
		var answer = parseInt(answerNumber) - 1;

		loading = true;

		if (typeof diagnosis === 'object') {
			var $textNode = $('<div class="m1-modal-s-highlight rf-white"><span class="m1-modal-s-title-tag-alt">'+diagnosis.diagnosis[answer].text1+'</span><span class="m1-modal-s-highlight-main">'+convertLineBreaks(diagnosis.diagnosis[answer].text2)+'</span><span class="m1-modal-s-highlight-sub">'+convertLineBreaks(diagnosis.diagnosis[answer].description)+'</span><span class="rf-mt-30 m1-modal-s-highlight-sub rf-mini">'+convertLineBreaks(diagnosis.diagnosis[answer].subtext)+'</span></div><div class="m1-modal-s-button-footer"><a href="#" data-prev="1" class="m1-modal-s-button js-back-button">再診断</a><!-- --><a href="'+diagnosis.diagnosis[answer].link1+getBVSessionId()+diagnosis.diagnosis[answer].link2+'&bgimg='+diagnosis.diagnosis[answer].bg+'" class="m1-modal-s-button-submit">ファンドを見る</a></div>');

			$('.m1-modal-s-content').css('background-image', 'url('+diagnosis.diagnosis[answer].bg+')')
			$modalContent.text('');
			$modalContent.append($textNode);

			// Change X icon to white
			$('.rf-icon-ui-x-disabled').removeClass('rf-icon-ui-x-disabled').addClass('rf-icon-ui-x-white');

			// Add custom button text
			if (diagnosis.diagnosis[answer].customSubmitText) {
				$('.m1-modal-s-button-submit').text(diagnosis.diagnosis[answer].customSubmitText);
			}

			setBackButtonEvents();

		} else {
			setErrorMessage();
		}

		loading = false;
	};

	// Set events

	// Show modal event
	$button.click(function(e) {
		siteCatalystCall();
		$backdrop.show();
		$modal.show();
		e.preventDefault();
	});

	// Hide modal event (and reset questions to 1)
	$closeButton.click(function(e) {
		$backdrop.hide();
		$modal.hide();
		loadQuestionUI()
		e.preventDefault();
	});

	if (window.location.hash === '#fund-quiz') {
		$backdrop.show();
		$modal.show();
	}

	// TODO:
	// for fidel's server: /client/c1/rakutenw/check/member/smt/json/fund/questions.json
	// for real server: /member/smt/json/fund/questions.json

	// Load question list into object
	$.ajax({  
		url: '/member/smt/json/fund/questions.json', 
		type: 'GET',
		success: function(data) {
			questions = data;
			loadQuestionUI();
		},
		error: function() {
			setErrorMessage();
		}
	});

	// TODO:
	// for fidel's server: /client/c1/rakutenw/check/member/smt/json/fund/diagnosis.json
	// for real server: /member/smt/json/fund/questions.json

	$.ajax({
		url: '/member/smt/json/fund/diagnosis.json',
		type: 'GET',
		success: function(data) {	
			diagnosis = data;
		},
		error: function() {
			diagnosis = null;
		}
	});

})(jQuery);
/*Site Catalyst call on click of Easy Search button*/
function siteCatalystCall()
{
	$.ajax({  
		url: 'smt_info_fu_choose_fund.do;BV_SessionID='+getBVSessionId()+'?eventType=init&l-id=mem_sp_fu_select_search_easy_fund_search', 
		type: 'POST',
		success: function(data) {
		},
		error: function() {		
		}
	});
}