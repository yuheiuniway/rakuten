//Showing flags icons in SP080101 foreign MMF section
$(document).ready(function() {
	if ($('#USD')) {
		$('span#USD').addClass('rf-icon-flag-us');
	}
	if ($('#EURO')) {
		$('span#EURO').addClass('rf-icon-flag-eu');
	}
	if ($('#GBP')) {
		$('span#GBP').addClass('rf-icon-flag-uk');
	}
	if ($('#AUD')) {
		$('span#AUD').addClass('rf-icon-flag-au');
	}
	if ($('#NZD')) {
		$('span#NZD').addClass('rf-icon-flag-nz');
	}
	if ($('#CAD')) {
		$('span#CAD').addClass('rf-icon-flag-ca');
	}
	if ($('#TRY')) {
		$('span#TRY').addClass('rf-icon-flag-tr');
	}
	if ($('#ZAR')) {
		$('span#ZAR').addClass('rf-icon-flag-za');
	}
	if ($('#RUB')) {
		$('span#RUB').addClass('rf-icon-flag-ru');
	}
	if ($('#MXN')) {
		$('span#MXN').addClass('rf-icon-flag-mx');
	}
	if ($('#SGD')) {
		$('span#SGD').addClass('rf-icon-flag-sg');
	}
	if ($('#HKD')) {
		$('span#HKD').addClass('rf-icon-flag-hk');
	}
	if ($('#MYR')) {
		$('span#MYR').addClass('rf-icon-flag-my');
	}
	if ($('#CNY')) {
		$('span#CNY').addClass('rf-icon-flag-cn');
	}
	if ($('#THB')) {
		$('span#THB').addClass('rf-icon-flag-th');
	}
	if ($('#IDR')) {
		$('span#IDR').addClass('rf-icon-flag-id');
	}
		
	

	$('.rf-accordion.rf-accordion-account.rf-separator:even').addClass('m1-no-border');
	$('.rf-accordion.rf-accordion-account.rf-separator').each(function() {
		var i=0;
		$('.rf-accordion.rf-accordion-account.rf-separator').each(function() {
			i++;
	    });
	    if(i % 2 == 0 ){
			$('.rf-pagination').first().css('border-top','1px solid #ddd');
	    } else {
			$('.rf-pagination').removeAttr('style');
	    }
	});
	
	
	
	
	if($('#increaseFlgOn').is(':checked') ){
		 
		 $( '#m1-bonus-panel' ).show();
	 }
	 if($( '#increaseFlgOff' ).is(':checked') ) {
	     $( '#m1-bonus-panel' ).hide();
	}
	
	/*Added tab button switch functionality for SP060901 screen*/
	$('#histTab1').click(function() {
		$('#setting_hist').show();
		$('#fund_hist').hide();
		$(this).addClass('active');
		$('#histTab2').removeClass('active');
	});
	$('#histTab2').click(function() {
		$('#setting_hist').hide();
		$('#fund_hist').show();
		$(this).addClass('active');
		$('#histTab1').removeClass('active');
	});
	
	getAsyncGlobalMenuUnreadMsgCount();

	errorMessageShow();
	
	 $(window).resize(function () {
		 errorMessageShow();
             });
	 /*Added autocomplete search box data functionality for SP020101  */
	 if($("#consign_Search_Input").length !== 0){
		 var availableTags =$.unique( getCookie('_searches').split(',')); 
		 $( "#consign_Search_Input" ).autocomplete({
			 source: function(request, response) {
			        var results = $.ui.autocomplete.filter(availableTags, request.term);
			        response(results.slice(0, 5));
			    }
	     });
	 }

     $('#efoNum').attr("placeholder", "10000");
});

/* Added JS Functionm to handle Key press event for Screen SP020101 */
function handleKeyPress(e) {
	if (e.keyCode === 13) {
		e.preventDefault(); // Ensure it is only this code that runs
		var val=$("#consign_Search_Input").val();
		var cookiesData = getCookie('_searches');
		var isElementPresent=-1;
		isElementPresent=cookiesData.indexOf(val);
		if(isElementPresent == -1){
			document.cookie = "_searches="+cookiesData+','+val;
		}
		$("#condition").val("ファンド名称like*"+val+"*");
		document.getElementById('searchConditionModal').submit();
	}
}

/*Added cookies for store search result for SP020101 page*/
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}


function clickedTab(id,url) {
	var active_tab_selector = $('.js-Tab-Slider > li.active > a').attr('href');
    var actived_nav = $('.js-Tab-Slider > li.active');
    actived_nav.removeClass('active');
    $(id).addClass('active');
    window.location = url;
}

////This is for SP061101 js code end here
////fuction for clicking delete button (SP03 series)
function deleteFund() {
   var result = confirm('選択された銘柄を削除します。\nよろしいですか？');
   if (result) {
	   if($('#form_v1').length !== 0)
		   {
		   if($('.js-myCheckBox').length !==0){
			   var i=0;
			   $('input[type=checkbox]').each(function () {
				   if ( $(this).is(':checked') ) {
					   var hiddenField = $('#myHiddenField_'+i),
			            val = hiddenField.val();
			        hiddenField.val("");
			        hiddenField.val("true");
				   }
				   else{
					   var hiddenField = $('#myHiddenField_'+i),
			            val = hiddenField.val();
			        hiddenField.val("false");
				   }
			   i++;
			   });
		   }

		   }
	   
	  	  $("#form_v1").submit();
			  alert('ご指定のファンドを削除しました。');       
   }
   
}

/*This function is written for the screen SP080201 and SP080301 filter condition part
 * This method is responsible for show fund list according to click of fund kind data.*/  
function setFundCode(val){
	$("#fund_select option[value!='']").remove();
	var dscrNmLst = $("input:hidden[name='dscrNm']");
	var fundKindCdLst = $("input:hidden[name='fundKindCd']");
	for(var i = 0;i < dscrNmLst.size(); i++) {
		var fundName=$(fundKindCdLst[i]).val();
	
		if(val==fundName && val!='A' ){
			var dscrNm = $(dscrNmLst[i]).val();
			if(dscrNm != null && dscrNm != undefined && dscrNm != "") {
				dispDscrNm = dscrNm.replace(/(<BR>|<br>)/gi,"&nbsp;&nbsp;");
			}
			$("#fund_select").append($("<option></option>").html(dispDscrNm).val(dscrNm));
		}
		if(val=='A'){
			var dscrNm = $(dscrNmLst[i]).val();
			if(dscrNm != null && dscrNm != undefined && dscrNm != "") {
				dispDscrNm = dscrNm.replace(/(<BR>|<br>)/gi,"&nbsp;&nbsp;");
			}
			$("#fund_select").append($("<option></option>").html(dispDscrNm).val(dscrNm));
		}
	}
}

//// Adding favorite fund registration method. 
function addFavouriteFund(commodity,inputCd,sonarDscrCd,marketCd,isinCd,timeout) {
	////Get selected radio button value from provided group.
    var selectedRadioBox = $("input:radio[name^=fundReg020502]:checked").val();

    var urlParam= "&commodity=" + commodity + "&inputCd=" + inputCd + "&sonarDscrCd=" + sonarDscrCd + "&marketCd=" + marketCd + "&isinCd=" + isinCd;
	
	//// checking parameter is null or not
    if (commodity == "") {
        urlParam = urlParam.replace("&commodity=", '');
    }
    if (inputCd == "") {
        urlParam = urlParam.replace("&inputCd=", '');
    }
    if (sonarDscrCd == "") {
        urlParam = urlParam.replace("&sonarDscrCd=", '');
    }
    if (marketCd == "") {
        urlParam = urlParam.replace("&marketCd=", '');
    }
    if (isinCd == "") {
        urlParam = urlParam.replace("&isinCd=", '');
    }
	
	//// Add fund ajax call.
	addPrcList( getBVSessionId(), urlParam, timeout,selectedRadioBox);
	return selectedRadioBox;
}

/*Added BVSessionId method for spw js and page jump js which is used for only HTML include section*/
function getBVSessionId(){
	var sessionId= $("input:hidden[name='sessionId']").val();
	return sessionId;
}

/*Error message pop up center alignment*/
function errorMessageShow(){
	if($('#hiddenError').val()=="true"){
	if($('#error_message').length !==0){
        $("body").css('overflow', 'hidden');
        $("body").addClass('m1-modal-open');
        $('#error_message').show();
        $('.m1-modal-overlay').show();
		}
	}
}

/*Adding function to handle Link from SP010101 and show pop up at load time*/
if ($('#fundQuiz').length !== 0 && $('#pageLoadPopup').length !== 0) {
    var isPopupOpen = $('#pageLoadPopup').val() == "open" ? true : false;
    var isPageLoadFirst = 0;
    if (isPageLoadFirst === 0 && isPopupOpen) {
        $("body").css('overflow', 'hidden');
        $("body").addClass('m1-modal-open');
        $('#fundQuiz').fadeIn(700);
        $('.m1-modal-backdrop').fadeIn(700);
        isPageLoadFirst = 1;
    }
}

function getAsyncGlobalMenuUnreadMsgCount() {
	$.ajax({
		url : '/app/smt_async_get_unread_msg_count.do;BV_SessionID=' + getBVSessionId(),
		type : 'POST',
		cache : 'false',
		timeout : 6000,
		success : function(data) {			
			if ('session_error' == data) {
				window.top.location.replace('https://www.rakuten-sec.co.jp/session_error.html');
				return false;
			}

			$('#globalMenuUnreadCntSmt').html(data);
			var cnt = $('#globalMenuUnreadCntSmt').find('.smt-menu-badgeBase').text();
			if (cnt) {
				cnt = cnt.replace(/(^\s+)|(\s+$)/g, "");
				$('#globalMenuUnreadCntSmt').html(cnt);
				$('#globalMenuUnreadCntSmt').addClass("rf-header-badge");
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			$('#globalMenuUnreadCntSmt').html('');
		}
	});
}

//SP050602 for submit button
function checkRadioValue() {
	
	if($('input[name="convCashFlg"]:checked').val() == '2') {
		$("#form1Cancel").submit();
	} 
	if($('input[name="convCashFlg"]:checked').val() == '1') {
		$("#form1Cancel").submit();
	}
}
//SP050602 radio button
$('input[name="convCashFlg"]').on('click', function () {
    if ($(this).val() === '2') {
        $('#return_cnf_scrn').removeAttr("disabled");
        
        //$('#efoNum').attr("disabled", "disabled");
        $('#efoNum').attr("disabled", "true");
        $('#efoNum').val('');
    }
    if ($(this).val() === '1') {
       $('#efoNum').removeAttr("disabled");
       $('#efoNum').attr("placeholder", "10000");
		var regex = /[0-9]|\./;
		var numValidation = '番号のみを入力';
		var num = $('#efoNum').val()
        $('#efoNum').on('keyup', function () {
			if(!regex.test($('#efoNum').val()) ) {
				$('#errMsg').show();
				$('#errMsg').text(numValidation);
			} else{
				$('#errMsg').hide();
			}
            $('#return_cnf_scrn').removeAttr("disabled");
            if ($('#efoNum').val().length == 0) {
				
                $('#return_cnf_scrn').attr("disabled", "disabled");
            } else {

                $('#return_cnf_scrn').removeAttr("disabled");
            }
        });
		$('#efoNum').on('mouseup', function () {
            $('#return_cnf_scrn').removeAttr("disabled");
            if ($('#efoNum').val().length == 0) {
           	$('#return_cnf_scrn').attr("disabled", "disabled");
            } else {
           	 $('#return_cnf_scrn').removeAttr("disabled");
            }
       });
	    if (num == 0) {
		    $('#return_cnf_scrn').attr("disabled", "disabled");
		}
     }
});

function closeErrordialog()
{
  $('#hiddenError').val("viewed");
  $('body').removeClass('m1-modal-open');
  $("body").css('overflow', 'auto');
  $('#error_message').hide();
  $('.m1-modal-overlay').hide();
}

// PDF links enabled/disabled: SP61001
function disabledMokuromiLink() {
	$(".mokuromiSection").css({"pointer-events":"none","cursor":"not-allowed","opacity":"0.5"});
	//alert('button disabled');
}

function disabledHokanLink() {
	//alert('button disabled');
	$(".hokanSection").css({"pointer-events":"none","cursor":"not-allowed","opacity":"0.5"});
}

function disabledReserveAgreementLink() {
	//alert('button disabled');
	$(".reserveAgreementLink").css({"pointer-events":"none","cursor":"not-allowed","opacity":"0.5"});
}

function disabledRsCardLitLink() {
	//alert('button disabled');
	$(".rCardLitLink").css({"pointer-events":"none","cursor":"not-allowed","opacity":"0.5"});
}

function disabledRsDebitLitLink() {
	//alert('button disabled');
	$(".rDebitLitLink").css({"pointer-events":"none","cursor":"not-allowed","opacity":"0.5"});
}

function enabledMokuromiLink() {
	$(".mokuromiSection").removeAttr("style");
	//alert('button enabled');
}

function enabledHokanLink() {
	$(".hokanSection").removeAttr("style");
	//alert('button enabled');
}

function enabledReserveAgreementLink() {
	$(".reserveAgreementLink").removeAttr("style");
	//alert('button enabled');
}
function enabledRsCardLitLink() {
	$(".rCardLitLink").removeAttr("style");
	//alert('button enabled');
}
function enabledRsDebitLitLink() {
	$(".rDebitLitLink").removeAttr("style");
	//alert('button enabled');
}

function resetDisplayCondition(){
	 $('#searchConditionModal')[0].reset();
}