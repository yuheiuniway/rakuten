$(document).ready(function(){
	var slider =$('.m1-bxslider-top .bxslider').bxSlider({
	    mode: 'horizontal',
	    auto: true,
	    infiniteLoop: true,
	    hideControlOnEnd: true,
	    controls: false
	});
	$('.m1-bxslider-bnr .bxslider').bxSlider({
	    mode: 'horizontal',
	    auto: true,
	    infiniteLoop: true,
	    hideControlOnEnd: true,
	    controls: false,
		preventDefaultSwipeY: true,
		touchEnabled: true
	});
});