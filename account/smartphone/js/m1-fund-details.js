//// On next tab click show that tab or on slide right and left show that tab.
if ($('.js-tab-menu ').length !== 0 && $('#tabsStickyNav').length !== 0) {
    var items = $('.js-tab-menu > li'),
    selectedIndex = 0,
    scroller = $(".numWrap"),
    scrollerWidth = scroller.width();
}
(function () {	
    // new Function for showing current Active Tab & hide InActive Tab for SP020501 page.
    if ($('#tabSticky').length !== 0) {
        var img, margin;
        new Hammer($("#MainDivForSwipe")[0], {
            domEvents: true
        });
    }
    document.body.addEventListener('touchstart', swipeLeftOrRightOnTouchStart, false);
    document.body.addEventListener('touchend', swipeLeftOrRightOnTouchEnd, false);

    //// On next tab click show that tab or on slide right and left show that tab.
    if ($('.js-tab-menu ').length !== 0 && $('#tabsStickyNav').length !== 0) {
        items.on('click', function (e) {
            selectedIndex = items.index($(this));
            selectItem();
        });
    }

    if ($('#t1').length !== 0) {
        //// This loop execute on only SP020501.html
        //// Used of this method to show next and previous arrows on first tab.
        showNextAndPrevArrow('#t1');
    }
    if ($('#tabsStickyNav').length !== 0) {
        ////if this id :tabsStickyNav not present on page not execute following code.
        $('#tabsStickyNav').tabs({
            collapsible: true,
            beforeActivate: function (event, ui) {
                var nextTabIndex, currentTabIndex;
                nextTabIndex = ui.newPanel.attr('id');
                currentTabIndex = ui.oldPanel.attr('id');
                if (typeof nextTabIndex === 'undefined' || typeof currentTabIndex === 'undefined') {
                    event.preventDefault();
                }
                if (nextTabIndex > currentTabIndex) {
                    $('#tabsStickyNav').tabs('option', 'hide', { effect: 'slide', direction: 'left', duration: 300 });
                }
                if (nextTabIndex < currentTabIndex) {
                    $('#tabsStickyNav').tabs('option', 'hide', { effect: 'slide', direction: 'right', duration: 300 });
                }
            }
        });
        if ($('#divTab1').length !== 0 || $('#divTab2').length !== 0 || $('#divTab3').length !== 0 || $('#divTab4').length !== 0 || $('#divTab5').length !== 0 || $('#divTab6').length !== 0 || $('#divTab7').length !== 0 || $('#divTab8').length !== 0) {
            //// hiding divs as it is showing on which tab currently user focuses.
            $('#divTab1').hide();
            $('#divTab2').hide();
            $('#divTab3').hide();
            $('#divTab4').hide();
            $('#divTab5').hide();
            $('#divTab6').hide();
            $('#divTab7').hide();
            $('#divTab8').hide();
        }
        swipeLeftOrRightOnTouchStart();
        //// on swiping on maindiv changed tabs.
        var MainDivForSwipe = document.getElementById('MainDivForSwipe');
        if (MainDivForSwipe !== null) {
	         Hammer(MainDivForSwipe).on('swipeleft', function () {
	            changeNavTab(true);
				// added the classes for the next tab show icons
				$('.m1-arrow-back').removeClass('is-show-back');
				$('.m1-arrow-next').addClass('is-show-next');
	        });
	        Hammer(MainDivForSwipe).on('swiperight', function () {
	            changeNavTab(false);
				// added the classes for the next tab show icons
				$('.m1-arrow-back').addClass('is-show-back');
				$('.m1-arrow-next').removeClass('is-show-next');
	        });
	    }
    }

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var $stickyNav = $('#stickyNav');
        if ($stickyNav.length !== 0) {
            $stickyNav.addClass('nav-fixed-top');
            if (scroll <= 10) {
                $stickyNav.removeClass('nav-fixed-top');
            }
        }
    });
    
    //// if number is 3 digit then making it small else it will be large.
    if ($('#digitBuy').length !== 0) {
        var $digit = $('#digitBuy').text();
        $digit = $digit.replace("��", "");
        var $legthDigit = $digit.length;
        if ($legthDigit >= 3) {
            $('#digitBuy').removeClass("rf-mega rf-mt-0 rf-mb-5").addClass("rf-large rf-mb-10");
        }
    }
})();
//// this is function is used for on click of tab switching arrow ex. next tab arrow or prev tab arrow to switch to next tab as tab index start from 0 but in html it is start from 1. 
function showTabs(currentTabIndex, nextTabIndex) {
    var $tabs = $('ul[data-role="nd2tabs"] >li > a');
    $tabs.eq(nextTabIndex - 1).click();
}
//// This method is for showing showing arrow next and previous on all tabs.
function showNextAndPrevArrow(targetTabIndex) {
    var i = 0;
    switch (targetTabIndex) {
        case '#t1':
            i = 1;
            break;
        case '#t2':
            i = 2;
            break;
        case '#t3':
            i = 3;
            break;
        case '#t4':
            i = 4;
            break;
        case '#t5':
            i = 5;
            break;
        case '#t6':
            i = 6;
            break;
        case '#t7':
            i = 7;
            break;
        case '#t8':
            i = 8;
            break;
    }
    $('#divTab_' + i).show().delay(5000).hide(0);
}
//// This method is for detect whether swipe right event occur or swipe left event.
function changeNavTab(left) {
    var $tabs = $('ul[data-role="nd2tabs"] >li > a');
    var len = $tabs.length;
    var curidx = 0;
    $tabs.each(function (idx) {
        if ($(this).closest("li").hasClass('active')) {
            curidx = idx;
        }
    });
    var nextidx = 0;
    if (left) {
        nextidx = (curidx >= len) ? 0 : curidx + 1;
    } else {
        nextidx = (curidx <= 0) ? len : curidx - 1;
    }
    $tabs.eq(nextidx).click();
}

////This class is used for moved to next tab for which tabId provided.
function moveToNextTab(tabId) {
	$(window).scrollTop(0);
    var $tabs = $('ul[data-role="nd2tabs"] >li > a');
    $tabs.eq(tabId).click();
}

function selectItem() {
    var selected = items.eq(selectedIndex);
	items.removeClass('active');
    selected.addClass('active');
    focus(selected.position().left + selected.width()*0.5);
	showNextAndPrevArrow(selected.find('a:first').attr("href"));
}

function focus(originalLeft) {
    scroll = originalLeft - (scrollerWidth / 2);
    scroller.stop().animate({
        scrollLeft: scroll
    }, 800);

}

// new Function for showing current Active Tab & hide InActive Tab for SP020501 page.
function swipeLeftOrRightOnTouchStart() {
    var eventHandlers = {
        panright: function () { },
        press: function () { },
        panleft: function () { }
    }
    $("#MainDivForSwipe").on("panright press panleft", function (ev) {
        $('.js-showTabOnLeftRight').css('display', 'block');
        if(ev.type === 'panright') {
			$('.m1-arrow-back').addClass('m1-arrow-back');
			$('.m1-arrow-next').removeClass('is-show-next');
			$('.m1-arrow-next').removeClass('is-show-next-transition');
		} else if((ev.type === 'panleft')){
			$('.m1-arrow-next').addClass('m1-arrow-next');
			$('.m1-arrow-back').removeClass('is-show-back');
			$('.m1-arrow-back').removeClass('is-show-back-transition');
		}
    });

}
function swipeLeftOrRightOnTouchEnd(ev) {
    $('.js-showTabOnLeftRight').delay(5000).hide(0)
}
