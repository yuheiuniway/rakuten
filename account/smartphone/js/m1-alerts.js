//// To redirect to particular location after close modal.
function registerFavoriteFund(location) {
    if (!alert('お気に入りファンドに登録しました。')) {
        document.location = location;
    }
}

//// Used to switch to tab on modal close and then to desired tab
function regNavScreenfromTab(val) {
    if (!alert('お気に入りファンドに登録しました。')) {
    	document.location = 'smt_info_fu_invest_detail.do?Tab=' + val;
    }
}