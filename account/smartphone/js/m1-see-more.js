(function () {
    $('.js-see-more').each(function () {
        var $this = $(this);
        var showChar = 29;
        var ellipse = '...';
        var moreText = '続きを読む';
        var lessText = '';
        var originalContent = $this.text();
        if (originalContent.length > showChar) {
            var shownContent = originalContent.substr(0, showChar);
            var html = shownContent + '<span class="m1-see-more-ellipse">' + ellipse + '</span>&nbsp;<span class="m1-see-more-content"><a href="" class="js-see-more-link m1-see-more-link">' + moreText + '</a></span>';
            $this.html(html);
            $this.find('.js-see-more-link').click(function (e) {
                $this.text(originalContent);
                e.preventDefault();
            });
        }
    });
})();