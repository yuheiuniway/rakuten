//// This timer variable is used for preventing multiple scroll event is fired.
var timer;
$(window).scroll(function () {
    if (timer) {
        window.clearTimeout(timer);
    }
    timer = window.setTimeout(function () {
        var scroll = $(window).scrollTop();
        var $stickyNav = $('#stickyNav');
        var $floatingButton = $('.js-floating-button');
        var $floatingButtonLargeScreen = $('.js-floating-button-large-screen');
        if ($stickyNav.length !== 0) {
            $stickyNav.addClass('nav-fixed-top');
            if (scroll <= 10) {
                $stickyNav.removeClass('nav-fixed-top');
            }
        }

        if ($floatingButton.length !== 0) {
            var height = $(document).height();
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();
            var minusHeight = height - 900;
            if (windowWidth >= 600 && windowWidth <= 1366) {
                //// this conditon is for tabs.
                if (windowHeight < 420) {
                    //// This condition is for mobile having width greater than 600 but height less than that 420 e.g iphone 6 plus. 
                    if ($(window).innerHeight() < $(window).innerWidth()) {
                        //// Landascape mode
                        minusHeight = height - 600;
                    }
                }
                else if (windowHeight >= 750) {
                    //// For tabs.
                    minusHeight = height - 1400;
                }
            }
            if (scroll >= minusHeight) {
                removeStickyFooterClass();
            }
            else {
                addStickyFooterClass();
            }
        }
        // this script is for large screen devices and if it's content are less 
        if ($floatingButtonLargeScreen.length !== 0) {
            var height = $(document).height();
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();
            var minusHeight = height - 830;
            if (windowWidth >= 600 && windowWidth <= 1366) {
                //// this conditon is for tabs.
                if (windowHeight < 420) {
                    //// This condition is for mobile having width greater than 600 but height less than that 420 e.g iphone 6 plus. 
                    if ($(window).innerHeight() < $(window).innerWidth()) {
                        //// Landascape mode
                        minusHeight = height - 600;
                    }
                }
                else if (windowHeight >= 750) {
                    //// For tabs.
                    minusHeight = height - 1400;
                }
            }
            if (scroll >= minusHeight) {
                removeStickyFooterClass();
            }
            else {
                addStickyFooterClass();
            }
        }

    }, 50);
});

//// When myModal i.e full screen modal opens on that modal add sticky footer and remove sticky footer on scroll events.
$('#myModal').scroll(function () {
    if (timer) {
        window.clearTimeout(timer);
    }
    timer = window.setTimeout(function () {
        var $modalFloatingButton = $('.js-mymodal-floating-button');
        var $modal = $('#myModal');
        if ($modalFloatingButton.length !== 0) {
            var scroll = $modal.scrollTop();
            var height = $modal.height();
            var windowWidth = $modal.width();
            var windowHeight = $(window).height();
            var minusHeight = height - 650;

            if (windowHeight >= 400 && windowHeight <= 480) {
                //// For mobiles with height within 400-480 e.g iphone 4
                minusHeight = height - 300;
            }
            if (windowHeight >= 600 && windowHeight <= 650) {
                //// For mobiles with height within 600-640 e.g Galaxy S5
                minusHeight = height - 600;
            }
            if (windowHeight >= 500 && windowHeight <= 599) {
                //// For mobiles with height within 600-640 e.g iphone 5
                minusHeight = height - 500;
            }
            if ($modal.innerHeight() < $modal.innerWidth()) {
                //// Landascape mode
                minusHeight = height - 100;
            }
            if (windowHeight > 650) {
                var minusHeight = height - 650;
            }
            if (scroll >= minusHeight) {
                $modalFloatingButton.removeClass('m1-sticky-footer');
            }
            else {
                $modalFloatingButton.addClass('m1-sticky-footer');
            }
        }
    }, 30);
});

//// adding m1-sticky-footer class
function addStickyFooterClass() {
    $('.js-floating-button').addClass('m1-sticky-footer');
}

//// removing m1-sticky-footer class
function removeStickyFooterClass() {
    $('.js-floating-button').removeClass('m1-sticky-footer');
}