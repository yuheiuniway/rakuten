(function() {
	// Date format in yyyy/mm/dd
	Date.prototype.yyyymmdd = function() {
		var yyyy = this.getFullYear().toString();
		var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
		var dd = this.getDate().toString();
		return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-"+ (dd[1] ? dd : "0" + dd[0]); // padding
	};

	// // on click selectChkbtn button need to checked other buttons.
	$('#selectChkbtnTwo').change(function() {
				$('#superChkDivTwo').find("input:checkbox").prop('checked',
						$(this).prop("checked"));
			});

	// // on click selectChkbtn button need to checked other buttons.
	$('#selectChkbtn').change(function() {
				$('#superChkDiv').find("input:checkbox").prop('checked',
						$(this).prop("checked"));
			});

	// // on click of child buttons select and deselect parent button
	$('.js-chekboxSelector').click(function() {
				var flagForCheckbox = 0;
				$('.js-chekboxSelector').each(function() {
					if (!this.checked) {
						flagForCheckbox = 1;
					}
				});
				$('#selectChkbtn').prop('checked',
						flagForCheckbox == 0 ? true : false);
			});

	// // on click of child buttons select and deselect parent button
	$('.js-chekboxSelector2').click(function() {
				var flagForCheckbox = 0;
				$('.js-chekboxSelector2').each(function() {
					if (!this.checked) {
						flagForCheckbox = 1;
					}
				});
				$('#selectChkbtnTwo').prop('checked',
						flagForCheckbox == 0 ? true : false);
			});

	$('#checknull').find('.js-myCheckBox').change(function() {
				var $parentCheckbox = $('#checknull');
				var $childCheckbox = $parentCheckbox.find('.js-myCheckBox');
				$('#confirmButton').prop('disabled',
						$childCheckbox.filter(':checked').length < 1);
			});
	if ($('#checknull').length !== 0) {
		$('#checknull').find('.js-myCheckBox').change();
	}

	// Set default date settings in PlaceHolder
	var $dtTo1 = $('#dtTo1');
	var $dtFrom1 = $('#dtFrom1');
	var $dtTo2 = $('#dtTo2');
	var $dtFrom2 = $('#dtFrom2');
	if ($dtTo1.length !== 0 || $dtFrom1.length !== 0 || $dtTo2.length !== 0
			|| $dtFrom2.length !== 0) {
		var today = new Date();
		var last = new Date(today.getTime() - (1 * 24 * 60 * 60 * 1000));
		$dtTo1.val(last.yyyymmdd());
		$dtFrom1.val(last.yyyymmdd());
		$dtTo2.val(last.yyyymmdd());
		$dtFrom2.val(last.yyyymmdd());
	}

	// // Initially specific options are shown but after slide up on this buton
	// is clicked show all options again.
	$('#m1-bonus-button').click(function() {
		$('#m1-bonus-panel').slideDown('normal');
	});

	// // on click hide i.e slideup specific options.
	$('#m1-nobonus-button').click(function() {
		$('#m1-bonus-panel').slideUp('normal');
	});

	$('#btnCalendar1').click(function() {
		$('#dvcalendar1').show();
		$('#dvcalendar2').hide();
	});

	$('#btnCalendar2').click(function() {
		$('#dvcalendar2').show();
		$('#dvcalendar1').hide();
	});

	// // start day less than end day
	$('#reserveDateFrom').change(function() {
		dayChecker();
	});

	$('#reserveDateTo').change(function() {
		dayChecker();
	});

	// // On nisseiFundname click search should slide up and header part need to
	// be hide
	$('#NisseiFundName').click(function() {
		$('#dvMyModalHeader').hide();
	});

	// // On nisseiFundname lost focus search should slide down and header part
	// need to be visible
	$('#NisseiFundName').focusout(function() {
		$('#dvMyModalHeader').show();
	});

	// This method is used to resolve android keyboard issue that was
	// conflicting with fixed footer when input button is focused//
	$('input[type=text]').on(
			'focus touchmove',
			function() {
				if ($('.rf-form-button-footer-border').hasClass(
						'm1-sticky-footer')) {
					$('.rf-form-button-footer-border').removeClass(
							'js-floating-button');
					$('.rf-form-button-footer-border').removeClass(
							'm1-sticky-footer');
				}
			});
	$('input[type=text]').on('blur', function() {
		$('.rf-form-button-footer-border').addClass('js-floating-button');
		$('.rf-form-button-footer-border').addClass('m1-sticky-footer');
	});

	// // on click of search area hide two divs.
	$('#SearchArea').click(function() {
		$('#scrolltoTop').fadeOut(100);
		$('#FundMainSection').fadeOut(100);
	});

})();

// //Reset form SP060701.html
function resetCheckField() {
	$('#searchConditionModal')[0].reset();
	// // Set values to null date field.
	$("input[type=date]").each(function resetDate() {
		this.value = '';
	});

	$("input[name='fundNm']").each(function resetDate() {
		this.value = '';
		$(this).attr("placeholder", "ファンド名を入力");
	});

	// // For hidding error div
	$('#ErrorDiv').hide();
	// // Set Combobox Default value
	$('#cbInvestment').val("a");
	$('#fundStatus').val("a");
	$('#paymentMethod').val("a");
	$('#reserveDateFrom').val('0');
	$('#reserveDateTo').val('0');

	$('#sortMode')[0].selected = true;

	if ($('input:radio[name=tradeType]').length > 0) {
		$('input:radio[name=tradeType]')[0].checked = true;
	}

	if ($('input:radio[name=termCd]').length > 0) {
		$('input:radio[name=termCd]')[0].checked = true;
	}
	if ($('input:radio[name=accountCd]').length > 0) {
		$('input:radio[name=accountCd]')[0].checked = true;
	}

	if ($('#reserveSts').length > 0) {
		$('#reserveSts').val("1");
	}

	if ($('#investSettleCd').length > 0) {
		$('#investSettleCd').val("99");
	}
}

// // This method is for setting default values.
function setDefaultDates(val) {
	var today = new Date();
	var yesterday = new Date(today.getTime() - (1 * 24 * 60 * 60 * 1000));
	var last = yesterday;
	switch (val) {
	case '1D':
		last = yesterday;
		break;
	case '1M':
		last.setMonth(yesterday.getMonth() - 1);
		break;
	case '3M':
		last.setMonth(yesterday.getMonth() - 3);
		break;
	case '6M':
		last.setMonth(yesterday.getMonth() - 6);
		break;
	case '12M':
		last.setMonth(yesterday.getMonth() - 12);
		break;
	}
	$('#dtTo1').val(yesterday.yyyymmdd());
	$('#dtFrom1').val(last.yyyymmdd());
	$('#dtTo2').val(yesterday.yyyymmdd());
	$('#dtFrom2').val(last.yyyymmdd());
}

// // If start day greater than end day then showing error msg.
// // here we used '!=' but if we use '!==' then it won't work as expected as it
// yield result true.
function dayChecker() {
	var day1 = $('#reserveDateFrom').val();
	var day2 = $('#reserveDateTo').val();
	if (day1 != 0 && day2 != 0) {
		if (parseInt(day1) > parseInt(day2)) {
			$('#ErrorDiv').show();
		} else {
			$('#ErrorDiv').hide();
		}
	} else {
		$('#ErrorDiv').hide();
	}
}

// //Method for form submission and prevent if error is occured
function formSubmission() {
	if ($('#ErrorDiv').css("display") == "block") {
		event.preventDefault();
	} else {
		document.getElementById('searchConditionModal').submit();
	}
}
