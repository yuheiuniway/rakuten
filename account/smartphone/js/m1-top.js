(function () {
    if ($('.m1-bxslider-top').length > 0) {
        $('.m1-bxslider-top .bxslider').bxSlider({
            mode: 'horizontal',
            auto: true,
            infiniteLoop: true,
            hideControlOnEnd: true,
            controls: false
        });
    }
    if ($('.m1-bxslider-bnr').length > 0) {
        $('.m1-bxslider-bnr .bxslider').bxSlider({
            mode: 'horizontal',
            auto: true,
            infiniteLoop: true,
            hideControlOnEnd: true,
            controls: false,
            preventDefaultSwipeY: true,
            touchEnabled: true
        });
    }
    if ($('.m1-carousel-overflow').length > 0) {
        $('.m1-carousel-overflow').each(function () {
            var $this = $(this);
            var $list = $this.find('.m1-carousel-overflow-list');
            var listItems = $list.find('li');
            var listWidth = listItems.length * 130 - 10;
            $list.css('width', listWidth + 'px');
        });
    }
    //// Code for attaching swipe right and swipe left event on news area.
    var NewsArea_1 = document.getElementById('NewsArea_1');
    var NewsArea_2 = document.getElementById('NewsArea_2');
    if (NewsArea_1 !== null) {
        Hammer(NewsArea_1).on('swipeleft', function () {
            showNavSections('#li_NewsArea_1', '#NewsArea_1', '#li_NewsArea_2', '#NewsArea_2');
        });
        Hammer(NewsArea_1).on('swiperight', function () {
            showNavSections('#li_NewsArea_1', '#NewsArea_1', '#li_NewsArea_2', '#NewsArea_2');
        });
    }
    if (NewsArea_2 !== null) {
        Hammer(NewsArea_2).on('swiperight', function () {
            showNavSections('#li_NewsArea_2', '#NewsArea_2', '#li_NewsArea_1', '#NewsArea_1');
        });
        Hammer(NewsArea_2).on('swipeleft', function () {
            showNavSections('#li_NewsArea_2', '#NewsArea_2', '#li_NewsArea_1', '#NewsArea_1');
        });
    }

    //// If browser back button is pressed then show last active tab.Hence used onpageshow event.
    //// On basis of hidden field id determining curren page.
    //// Also for selection of tab click show active tab
    window.onpageshow = function (event) {
        if ($('#top').length !== 0) {
            pageMenuSelector("#dvMenu_2", "#dvMenu_3", $("#dvMenu_1"));
        }
        if ($('#choose').length !== 0) {
            pageMenuSelector("#dvMenu_1", "#dvMenu_3", $("#dvMenu_2"));
        }
        if ($('#favourite').length !== 0) {
            pageMenuSelector("#dvMenu_1", "#dvMenu_2", $("#dvMenu_3"));
        }
        if ($('#OrderMenu').length !== 0) {
            if ($('#OrderMenu').val() === "true") {
                pageMenuSelector("#dvExpandedMenu_2", "#dvExpandedMore", $("#dvExpandedMenu_1"));
            }
        }
        if ($('#AssetMenu').length !== 0) {
            if ($('#AssetMenu').val() === "true") {
                pageMenuSelector("#dvExpandedMenu_1", "#dvExpandedMore", $("#dvExpandedMenu_2"));
            }
        }
    };
})();
//====================================================================================
//// This method is for showing navigation section on swipe right and swipe left event.
function showNavSections(hideElement1, hideElement2, showElement1, showElement2) {
    $(hideElement1).removeClass('active');
    $(showElement1).addClass('active');
    $(hideElement2).hide();
    $(showElement2).show();
}

////This methods is for hiding div on button click and both function used to merge two pages in one page.
function redirectToPage2() {
	var cname = '0';
        $('#m1-fund-top-investment-area-ForPage1').hide();
        $('#m1-fund-top-investment-area-ForPage2').show();
        setCookie(cname);       
}

//<<RSEC_WM DEV SBP Date(20170424)>> method to set cookie to show no display state <<RSEC_WM DEV SBP Date(20170424) end >>
function redirectToPage1() {
	var cname = '1';
        $('#m1-fund-top-investment-area-ForPage2').hide();
        $('#m1-fund-top-investment-area-ForPage1').show();
        setCookie(cname);
}

//// This class remove classes from anchor tag for provided div.
function removeClassFromAnchor(divId_1, divId_2, className) {
    $(divId_1).find("a:first").removeClass(className);
    $(divId_2).find("a:first").removeClass(className);
}

//// on after page show to user show acive menu and active expanded menu if any.
function pageMenuSelector($menu_1, $menu_2, $addLinkMenu) {
    removeClassFromAnchor($menu_1, $menu_2, "rf-header-category-link--active");
    $addLinkMenu.find('a:first').addClass("rf-header-category-link--active");
}
