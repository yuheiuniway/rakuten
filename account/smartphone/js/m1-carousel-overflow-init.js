$(document).ready(function(){
	if ($('.m1-carousel-overflow').length > 0) {
		$('.m1-carousel-overflow').each(function() {
			var $this = $(this);
			var $list = $this.find('.m1-carousel-overflow-list');
			var listItems = $list.find('li');
			var listWidth = listItems.length * 130 - 10;

			$list.css('width', listWidth + 'px');
		});
	}
});