(function($) {

	var $analystArea = $('#analyst-area');
	var $analystCopy = $('#analyst-copy');
	var $analystPeriod = $('#analyst-period');
	var $analystTitle = $('#analyst-title');
	var $analystName = $('#analyst-name');
	var $analystImage = $('#analyst-image');
	var $analystComment = $('#analyst-comment');
	var $selectionArea = $('#selection-area');

	var loadAnalystData = function(data) {
		$analystCopy.html(data.copy);
		$analystPeriod.html(data.period);
		$analystTitle.html(data.title);
		$analystName.html(data.name);
		$analystImage.attr('src', data.image);
		$analystComment.html(data.comment);
	};

	var loadSelectionData = function(data) {
		var listLength = data.length;
		for (var i = 0; i < listLength; i++) {
			var item = data[i];
			var $node = null;
			var html = '<div class="rf-grid rf-grid--1"> \
	            <div class="rf-grid-column"> \
	              <a class="rf-link-block rf-font-bold rf-mb-0 rf-mt-20" href="#" onclick="'+ item.detail_onclick +'">' + item.name + '</a> \
	       		  <p class="rf-mv-0 rf-mini rf-font-bold">'+ item.company +'</p> \
	              <h4 class="rf-normal rf-font-bold rf-mb-5">ファンドスコア</h4> \
	            </div> \
	          </div>';
	        html += '<div class="rf-grid rf-grid--1"> \
	            <div class="rf-grid-column"> \
	              <div class="rf-grid rf-grid--4">';
	        html += addRating(item, 1);
	        html += addRating(item, 3);
	        html += addRating(item, 5);
	        html += addRating(item, 10);
	        html += '</div> \
              <hr class="rf-ht-dotted rf-mt-10 rf-mb-0"> \
            </div>';
            html += '<div class="rf-grid-column"> \
              <h4 class="rf-normal rf-font-bold rf-mb-5">ここがポイント!</h4> \
              <p class="rf-mt-0 rf-mb-20">'+ item.description +'</p> \
            </div>';
            html += '</div>';
            html += '<!-- Gray button section starts here --> \
          <div class="rf-bg-gainsboro m1-border-bottom-silver rf-pt-10 rf-pb-10"> \
            <div class="rf-grid rf-grid--1"> \
              <div class="rf-grid-column"> \
                <div class="rf-media"> \
                  <div class="rf-media-left"> \
                    <img class="rf-media-object m1-image-border" src="'+ item.image +'" width="69" height="96" alt=""> \
                  </div> \
                  <div class="rf-media-body"> \
                    <a href="#" onclick="'+ item.detail_onclick +'" class="rf-button rf-mt-0" role="button">個別銘柄の詳細へ</a> \
                    <div class="rf-form-button-group-double"> \
                      <div class="rf-form-button-group-inner"> \
                        <a href="#" onclick="'+ item.buy_onclick +'" class="rf-button-positive rf-small rf-pt-5 rf-pb-5" role="button">買い</a> \
                      </div> \
                      <div class="rf-form-button-group-inner"> \
                        <a href="#" onclick="'+ item.establish_onclick +'" class="rf-button-primary rf-small rf-pt-5 rf-pb-5" role="button">積立</a> \
                      </div> \
                    </div> \
                  </div> \
                </div> \
              </div> \
            </div> \
          </div> \
          <!-- Gray button section ends here --> \
          <!-- Detailed section ends here -->';
	        $node = $(html);
	        $('#selection-'+item.category).append($node);
		}
	};

	var addRating = function(item, years) {
		var html = '<div class="rf-grid-column rf-ph-0 rf-align-center"> \
	                  <p class="rf-mini rf-mt-0 rf-mb-5">'+ years +'年</p>';
	    var prop = 'score_'+years;
		if (item[prop] !== 5 && item[prop] !== 4 && item[prop] !== 3 && item[prop] !== 2 && item[prop] !== 1 && item[prop] !== '5' && item[prop] !== '4' && item[prop] !== '3' && item[prop] !== '2' && item[prop] !== '1') {
			html += '<p class="rf-mv-0">&mdash;</p>';
		} else {
			html += '<p class="rf-color-blue rf-font-bold rf-mv-0">'+ item[prop] +'</p>';
		}
		html += writeStars(item[prop]);
		html += '</div>';
		return html;
	};

	var writeStars = function(number) {
		var html = '';
		var blueStars = Number(number);
		var silverStars;
		if (blueStars !== 5 && blueStars !== 4 && blueStars !== 3 && blueStars !== 2 && blueStars !== 1 && blueStars !== '5' && blueStars !== '4' && blueStars !== '3' && blueStars !== '2' && blueStars !== '1') {
			silverStars = 5;
		} else {
			silverStars = 5 - blueStars;
		}
		html += '<div class="rf-rating rf-mini">';
		for (var i = 0; i < silverStars; i++) {
			html += '<span aria-hidden="true" class="rf-icon-ui-book-silver"></span>';
		}
		for (var e = 0; e < blueStars; e++) {
			html += '<span aria-hidden="true" class="rf-icon-ui-book-link"></span>';
		}
		html += '</div>';
		return html;
	};

	var setErrorMessage = function(area) {
		var $node = $('<p class="rf-pt-10 rf-pb-10 rf-align-center">データの読み込みにエラーがありました。<br>しばらくしてからもう一度お試しください。</p>');
		area.text('');
		area.append($node);
	};

	$.ajax({
		url: '/member/html/smt/json/fund/fund_analyst.json',
		type: 'GET',
		success: function(data) {
			loadAnalystData(data[0]);
		},
		error: function() {
			setErrorMessage($analystArea);
		}
	});

	$.ajax({
		url: '/member/html/smt/json/fund/fund_selection.json',
		type: 'GET',
		success: function(data) {
			loadSelectionData(data);
		},
		error: function() {
			setErrorMessage($selectionArea);
		}
	});

})(jQuery);