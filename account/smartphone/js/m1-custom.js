/* -----------------------------------------------------------------------------------------------------------
Rakuten Styling Foundation: Custom Js Code  Here
----------------------------------------------------------------------------------------------------------- */
//// on click of this check box id Intially disabled button need to show.
$('#checknull').find('.js-myCheckBox').change(function () {
        var $parentCheckbox = $('#checknull');
        var $childCheckbox = $parentCheckbox.find('.js-myCheckBox');
        $('#confirmButton').prop('disabled', $childCheckbox.filter(':checked').length < 1);
});
$('#checknull').find('.js-myCheckBox').change();

//// adding m1-sticky-footer class
function addStickyFooterClass() {
        $('.js-floating-button').addClass('m1-sticky-footer');
}

//// removing m1-sticky-footer class
function removeStickyFooterClass() {
        $('.js-floating-button').removeClass('m1-sticky-footer');
}

// Function for showing current Active Tab & hide InActive Tab
$(document).ready(function () {

	if($('#backButtonPressed').length !== 0) {
		if ( window.history.pushState) {
			window.history.pushState('forward', null, currentModal);
			history.pushState(null, null, location.href);
			$(window).on('popstate', function(event) {
				if(event.state!==null) {
					$('.m1-modal').hide();
					$('body').removeClass('m1-modal-open');
					$('body').css('overflow','scroll');
				}
			});
			history.go(1);
		}
	}
	// new Function for showing current Active Tab & hide InActive Tab for SP020501 page.
	if($('#tabSticky').length !== 0) {
		var img, margin;
		new Hammer( $( "#MainDivForSwipe" )[ 0 ], {
		  domEvents: true
		} );
	}
	document.body.addEventListener('touchstart', swipeLeftOrRightOnTouchStart, false);
	document.body.addEventListener('touchend', swipeLeftOrRightOnTouchEnd, false);
		if($('#tabSticky').length > 0) {
			$('.stickyHeader').removeAttr('id');
			$('.rf-header-category').removeAttr('id');
			$('.stickyTabs').attr('id','sticky-anchor');
			$('.stickyNav').attr('id','sticky');
			$('.rf-tabs').css({
				'background':'#fff'
			});
			$('.rf-tabs ul').css('width','97%');
		}	
        //// On next tab click show that tab or on slide right and left show that tab.
		if($('.js-tab-menu ').length !== 0 && $('#tabsStickyNav').length !== 0) {
			items.on('click', function(e) {
				selectedIndex = items.index($(this));
				selectItem();
			});
		}
        if ($('#t1').length !== 0) {
            //// This loop execute on only SP020501.html
            //// Used of this method to show next and previous arrows on first tab.
            showNextAndPrevArrow('#t1');
        }
		
		 var isModalOpened;
		//// For showing full screen modal and apply scroll to it so other content visible
        if ($('.js-windowModal').length !== 0) {
            isModalOpened = 1;
            setHeight();
        }
        
         //// if window switched to landscape mode or portrait mode then neeed to position modal again.
        $(window).resize(function () {
            //// If modal is closed or user dont open any modal then to prevent execution of this method this if loop is added.
            if (isModalOpened !== 0 && isModalOpened !== undefined) {
                setHeight();
            }
        });
    
        //This is used to Manage the scroll on click of Cancel and Register buttons on the same page.
        //====================================================
		//Default date settings in PlaceHolder
        //Checking following id present in page or not  if present binding values
		if ($('#dtTo1').length !== 0 || $('#dtFrom1').length !== 0 || $('#dtTo2').length !== 0 || $('#dtFrom2').length !== 0) {
			var today = new Date();
			var last = new Date(today.getTime() - (1 * 24 * 60 * 60 * 1000));
			$('#dtTo1').val(last.yyyymmdd());
			$('#dtFrom1').val(last.yyyymmdd());
			$('#dtTo2').val(last.yyyymmdd());
			$('#dtFrom2').val(last.yyyymmdd());
		}

		  //// if number is 3 digit then making it small else it will be large.
		  if ($('#digitBuy').length !== 0) {
			  var $digit = $('#digitBuy').text();
			  $digit = $digit.replace("位", "");
			  var $legthDigit = $digit.length;
			  if ($legthDigit >= 3) {
				  $('#digitBuy').removeClass("rf-mega rf-mt-0 rf-mb-5").addClass("rf-large rf-mb-10");
			  }
		  }
  
        //// Initially specific options are shown but after slide up on this buton is clicked show all options again.
		$( '#m1-bonus-button' ).click(function() {
		     $( '#m1-bonus-panel' ).slideDown( 'normal' );
		});

        //// on click hide i.e slideup specific options.
		$( '#m1-nobonus-button' ).click(function() {
		     $( '#m1-bonus-panel' ).slideUp( 'normal' );
		});
	    ////=============SP020101==========================================
        //// on click of search area hide two divs.
		$('#SearchArea').click(function () {
		    $('#scrolltoTop').fadeOut(100);
		    $('#FundMainSection').fadeOut(100);
		});
	    ////=============SP020101==========================================
	    ////=============For showing dropdown menu onclick=================
        //// Showing and hiding dropdown menu on click of dropdown
		if ($('.js-dropdown').length !== 0) {
		    $('.js-dropdown').each(function () {
		        var $this = $(this);
		        var $button = $this.find('button');
		        var $menu = $this.find('.rf-dropdown--menu');
		        var $title = $this.find('.js-dropdown-title');
		        var menuItems = $menu.find('li');
		        $button.click(function () {
		            $this.toggleClass('rf-open');
		        });
		        // Change active item and update dropdown title
		        menuItems.each(function () {
		            var $this = $(this);
		            $this.click(function () {
		                if (!$this.hasClass('rf-dropdown-selected')) {
		                    $('.rf-dropdown-selected').removeClass('rf-dropdown-selected');
		                    $this.addClass('rf-dropdown-selected');
		                    var text = $this.text().split('&nbsp;')[0];
		                    $title.text(text);
							changeSort($(this).val());
		                }
		            });
		        });
		    });
		}
        ///================For SP010101.html and SP010102.html=============
        //// Code for attaching swipe right and swipe left event on news area.
		var NewsArea_1 = document.getElementById('NewsArea_1');
		var NewsArea_2 = document.getElementById('NewsArea_2');
		if (NewsArea_1 !== null) {
		    Hammer(NewsArea_1).on('swipeleft', function () {
		        showNavSections('#li_NewsArea_1', '#NewsArea_1', '#li_NewsArea_2', '#NewsArea_2');
		    });
		    Hammer(NewsArea_1).on('swiperight', function () {
		        showNavSections('#li_NewsArea_1', '#NewsArea_1', '#li_NewsArea_2', '#NewsArea_2');
		    });
		}

		if (NewsArea_2 !== null) {
		    Hammer(NewsArea_2).on('swiperight', function () {
		        showNavSections('#li_NewsArea_2', '#NewsArea_2', '#li_NewsArea_1', '#NewsArea_1');
		    });
		    Hammer(NewsArea_2).on('swipeleft', function () {
		        showNavSections('#li_NewsArea_2', '#NewsArea_2', '#li_NewsArea_1', '#NewsArea_1');
		    });
		}
        //=========================================================================
        //=======================================SP010101.html=====================
        //// Show Menu for click on current menu if clicked again then hide menu.
		$('#dvExpandedMenu_1').click(function () {
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_2').hide();
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_3').hide();
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_1').toggle();
		});

		$('#dvExpandedMenu_2').click(function () {
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_1').hide();
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_3').hide();
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_2').toggle();
		});

		$('#dvExpandedMore').click(function () {
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_2').hide();
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_1').hide();
		    $('.rf-header-category-menu-inner.js-dvExpandedMenu_3').toggle();
		});
        //=======================================================================
        //// If browser back button is pressed then show last active tab.Hence used onpageshow event.
        //// On basis of hidden field id determining curren page.
        //// Also for selection of tab click show active tab
		window.onpageshow = function (event) {
		    if ($('#top').length !== 0) {
		        pageMenuSelector("#dvMenu_2", "#dvMenu_3", $("#dvMenu_1"));
		    }
		    if ($('#choose').length !== 0) {
		        pageMenuSelector("#dvMenu_1", "#dvMenu_3", $("#dvMenu_2"));
		    }
		    if ($('#favourite').length !== 0) {
		        pageMenuSelector("#dvMenu_1", "#dvMenu_2", $("#dvMenu_3"));
		    }
		    if ($('#OrderMenu').length !== 0) {
		        if ($('#OrderMenu').val() === "true") {
		            pageMenuSelector("#dvExpandedMenu_2", "#dvExpandedMore", $("#dvExpandedMenu_1"));
		        }
		    }
		    if ($('#AssetMenu').length !== 0) {
		        if ($('#AssetMenu').val() === "true") {
		            pageMenuSelector("#dvExpandedMenu_1", "#dvExpandedMore", $("#dvExpandedMenu_2"));
		        }
		    }
		};
        //=======================================================================
        //// For showing menu on click of menu
		$('#liMenuModal').click(function (e) {
		    $("body").addClass('rf-header-modal-open');
		    e.stopImmediatePropagation();
		});

		//// For Hiding attachment icon on click of edit button for SP030101.html Edit Modal
		$('.js-fav-edit-modal').click(function (e) {
		    $("#myModal").css('z-index', 99999);
		});

        //// On click of close button close menu.
		$('.rf-header-modal-close').click(function () {
		    $("body").removeClass('rf-header-modal-open');
		});

        //// On click of page other than opened modal (ex.modal opened for menu) need to hide.
		$("body").click(function (event) {
		    if ($("body").hasClass('rf-header-modal-open')) {
		        $("body").removeClass('rf-header-modal-open');
		    }
		});

        //// on click of menu stop body click event
		$('#dvMenuModal').click(function (event) {
		    event.stopPropagation();
		});
        //=======================================End SP010101.html=================
        //===========================SP040301======================================
        //// on click of id  changed status of that div and setting color so tha it is identified that user visited.
		$('#m1-anchor-linkpdf').click(function () {
		    $('#m1-anchor-linkpdf .m1-read-status').text('閲覧済');
		    $('#m1-anchor-linkpdf .m1-read-status').css({ "color": "#999999", "font-weight": "bold" });
		    $('#m1-anchor-linkpdf .m1-pdf-box').css("background-color", "#f4f4f4");
		});
        //========================================================================
        //// If js-more is not present on current page then not execute folowing code or vice versa.
        //// following code is for hiding more text by showing link if user click on link then showing that text.
		if ($('.js-more').length > 0) {
		    var showChar = 29;
		    var ellipsestext = "...";
		    var moretext = '続きを読む';
		    var lesstext = "";
		    $('.js-more').each(function () {
		        var content = $(this).html();
		        if (content.length > showChar) {
		            var c = content.substr(0, showChar);
		            var h = content.substr(showChar - 1, content.length - showChar);
		            var html = c + '<span class="moreelipses">' + ellipsestext + '</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
		            $(this).html(html);
		        }
		    });
		    $('.morelink').click(function () {
		        if ($(this).hasClass('less')) {
		            $(this).removeClass('less');
		            $(this).html(moretext);
		        } else {
		            $(this).addClass("");
		            $(this).html(lesstext);
		        }
		        $(this).parent().prev().toggle();
		        $(this).prev().toggle();
		        return false;
		    });
		}
        //===========================SP080201=====================================
        //// On click hide and show specific div.
		$('#btnCalendar1').click(function () {
		    $('#dvcalendar1').show();
		    $('#dvcalendar2').hide();
		});

		$('#btnCalendar2').click(function () {
		    $('#dvcalendar2').show();
		    $('#dvcalendar1').hide();
		});

        //// When we open the popup and type some text in textbox then cursor of the textbox scrolls with the page
		$('#myModal').scroll(function () {
		    $('#test').css('cursor', 'none');
		});

        //// Fot changing tab active status this code is used.
		$('.m1-search-result-nav > li > a ').click(function (e) {
		    e.preventDefault();
		    $('.m1-search-result-nav').find("li").find("a.active").removeClass('active');
		    $(this).addClass('active');
		});

        //// on click of this modal opens hence disable scrolling of body.
		$('.js-hidden-scroll').click(function () {
		    $("body").addClass('m1-modal-open');
		});

        //// Showing modal on click of js-ranking-modal.
		$('.js-ranking-modal').click(function (e) {
			
		    currentModal = '#ranking';
		    $('#ranking').css("display", "block");
		    $('#ranking').css("aria-hidden", "false");
			$("body").css('overflow', 'hidden');
			$("body").addClass('m1-modal-open');
			$('.m1-modal-overlay').show();
			e.preventDefault();
		});

        //// Showing modal on click of js-basicSearch-modal.
		$('.js-favourite-modal').click(function (e) {			 
			e.preventDefault();
			if ($('#basicSearch').length !== 0) {
			    currentModal = '#basicSearch';
		    	$('#basicSearch').css("display", "block");
		    	$('#basicSearch').css("aria-hidden", "false");
				$("body").css('overflow', 'hidden');
				$("body").addClass('m1-modal-open');
				$('.m1-modal-overlay').show();
			}
			if ($('#oprationResult').length !== 0) {
			    currentModal = '#oprationResult';
		    	$('#oprationResult').css("display", "block");
		    	$('#oprationResult').css("aria-hidden", "false");
				$("body").css('overflow', 'hidden');
				$("body").addClass('m1-modal-open');
				$('.m1-modal-overlay').show();
			}
		});

        // enable or disable "To the confirmation screen" button page SP050602 
		$('input[name="fundReg020502"]').on('click', function () {
		    if ($(this).val() === '1') {
		        $('#return_cnf_scrn').removeAttr("disabled");
		        $('#efoNum').attr("disabled", "disabled");
		        $('#efoNum').val('');
		    }
		    if ($(this).val() === '2') {
                $('#efoNum').removeAttr("disabled");
				var regex = /[0-9]|\./;
				var numValidation = '番号のみを入力';
				var num = $('#efoNum').val()
                $('#efoNum').on('keyup', function () {
					if(!regex.test($('#efoNum').val()) ) {
						$('#errMsg').show();
						$('#errMsg').text(numValidation);
					} else{
						$('#errMsg').hide();
					}
	                $('#return_cnf_scrn').removeAttr("disabled");
	                if ($('#efoNum').val().length == 0) {
		                $('#return_cnf_scrn').attr("disabled", "disabled");
	                } else {
		                $('#return_cnf_scrn').removeAttr("disabled");
	                }
                });
				$('#efoNum').on('mouseup', function () {
	                $('#return_cnf_scrn').removeAttr("disabled");
	                if ($('#efoNum').val().length == 0) {
		                $('#return_cnf_scrn').attr("disabled", "disabled");
	                } else {
		                $('#return_cnf_scrn').removeAttr("disabled");
	                }
				});
				if (num == 0) {
				    $('#return_cnf_scrn').attr("disabled", "disabled");
				}
		    }
		});

        //// on click selectChkbtn button need to checked other buttons.
		/*$('#selectChkbtnTwo').change(function () {
		    $('#superChkDivTwo').find("input:checkbox").prop('checked', $(this).prop("checked"));
			   
		});*/
		$("#selectChkbtn").click(function () {
		  $('.js-unCheckSelectAll').attr('checked', this.checked);
		});

		$(".js-unCheckSelectAll").click(function(){

			if($(".js-unCheckSelectAll").length == $(".js-unCheckSelectAll:checked").length) {
				$("#selectChkbtn").attr("checked", "checked");
			} else {
				$("#selectChkbtn").removeAttr("checked");
			}

		});

        //// on click selectChkbtn button need to checked other buttons.
		$('#selectChkbtn').change(function () {
		    $('#superChkDiv').find("input:checkbox").prop('checked', $(this).prop("checked"));
		});

        //// on click of child buttons select and deselect parent button
		$('.js-chekboxSelector').click(function () {
		    var flagForCheckbox = 0;
		    $('.js-chekboxSelector').each(function () {
                if (!this.checked) {
                    flagForCheckbox = 1;
		        }
		    });
		    $('#selectChkbtn').prop('checked', flagForCheckbox == 0 ? true : false);
		});

         //// on click of child buttons select and deselect parent button
		$('.js-chekboxSelector2').click(function () {
		    var flagForCheckbox = 0;
		    $('.js-chekboxSelector2').each(function () {
		        if (!this.checked) {
		            flagForCheckbox = 1;
		        }
		    });
		    $('#selectChkbtnTwo').prop('checked', flagForCheckbox == 0 ? true : false);
		});

        //// on click of favmodal button need to open mymodal which is of fullscreen.
		$('#favModal, #favModal_bottom, #favModal2, #favModal3').click(function () {
		    var modal = $('#myModal');
		    $("body").css('overflow', 'hidden');
		    $("body").addClass('m1-modal-open');
		    modal.show();
		});

        //// Added this funcation for Open FundQuiz modal
		$('.js-fundQuizModal').click(function () {
		    $("body").css('overflow', 'hidden');
		    $("body").addClass('m1-modal-open');
		    $('#fundQuiz').fadeIn(700);
		    $('.m1-modal-backdrop').fadeIn(700);
		});

        //// Added this funcation for Closed FundQuiz modal
		$('.m1-modal-s-close').click(function () {
		    $("body").css('overflow', 'auto');
		    $("body").removeClass('m1-modal-open');
		    $('#fundQuiz').fadeOut(700);
		    $('.m1-modal-backdrop').fadeOut(700);
		});

        //// Class close to js-close on click of that need to close modal opened during fav modal.
		$('.js-close').click(function () {
			isModalOpened = 0;
		    var modal = $('#myModal');
		    $("body").removeClass('m1-modal-open');
		    $("body").css('overflow', 'auto');
		    modal.hide();
		});
        
        //// On nisseiFundname click search should slide up and header part need to be hide
		$('#NisseiFundName').click(function () {
		    $('#dvMyModalHeader').hide();
		});

         //// On nisseiFundname lost focus search should slide down and header part need to be visible
		$('#NisseiFundName').focusout(function () {
		    $('#dvMyModalHeader').show();
		});

        //// start day less than end day
		$('#reserveDateFrom').change(function () {
		    DayChecker();
		});
		$('#reserveDateTo').change(function () {
		    DayChecker();
		});

        //// If PinCode less than or more than 4 digit then button should be disable for exact match 4 digit it should be enabled.
		$('#pin_id').keyup(function () {
		    if ($(this).val().length == 4) {
		        $('#can_odr').attr('disabled', false);
			} else if ($(this).val().length == 0) {
			    $('#can_odr').attr('disabled', true);
			}
		    else{
		        $('#can_odr').attr('disabled', true);
			}
		});

        //=======================================================================
        //// beforactivate event of tab from Jquery UI used for showing next tab switching animation.
		if ($('#tabsStickyNav').length !== 0) {
		    ////if this id :tabsStickyNav not present on page not execute following code.
		    $('#tabsStickyNav').tabs({
		        collapsible: true,
		        beforeActivate: function (event, ui) {
		            var nextTabIndex, currentTabIndex;
		            nextTabIndex = ui.newPanel.attr('id');
		            currentTabIndex = ui.oldPanel.attr('id');
		            if (typeof nextTabIndex === 'undefined' || typeof currentTabIndex === 'undefined') {
		                event.preventDefault();
		            }
		            if (nextTabIndex > currentTabIndex) {
		                $('#tabsStickyNav').tabs('option', 'hide', { effect: 'slide', direction: 'left', duration: 300 });
		            }
		            if (nextTabIndex < currentTabIndex) {
		                $('#tabsStickyNav').tabs('option', 'hide', { effect: 'slide', direction: 'right', duration: 300 });
		            }
		        }
		    });
		    if ($('#divTab1').length !== 0 || $('#divTab2').length !== 0 || $('#divTab3').length !== 0 || $('#divTab4').length !== 0 || $('#divTab5').length !== 0 || $('#divTab6').length !== 0 || $('#divTab7').length !== 0 || $('#divTab8').length !== 0) {
		        //// hiding divs as it is showing on which tab currently user focuses.
		        $('#divTab1').hide();
		        $('#divTab2').hide();
		        $('#divTab3').hide();
		        $('#divTab4').hide();
		        $('#divTab5').hide();
		        $('#divTab6').hide();
		        $('#divTab7').hide();
		        $('#divTab8').hide();
		    }
			swipeLeftOrRightOnTouchStart();
		    //// on swiping on maindiv changed tabs.
		    var MainDivForSwipe = document.getElementById('MainDivForSwipe');
		    if (MainDivForSwipe !== null) {
		        Hammer(MainDivForSwipe).on('swipeleft', function () {
		            changeNavTab(true);
					// added the classes for the next tab show icons
					$('.m1-arrow-back').addClass('is-show-back');
					$('.m1-arrow-next').addClass('is-show-next');
		        });
		        Hammer(MainDivForSwipe).on('swiperight', function () {
		            changeNavTab(false);
					// added the classes for the next tab show icons
					$('.m1-arrow-back').addClass('is-show-back');
					$('.m1-arrow-next').addClass('is-show-next');
		        });
		    }
		}

         //on click of outside window close modal
		$(window).on('click touchstart', function (event) {
		    if (currentModal !== "" && currentModal !== undefined)
		    {
		        var modal = document.getElementById(currentModal.replace("#", ""));
		        if (event.target == modal) {
		            setClassToDisplayNone(currentModal);
		        }
		    }
		});
});

//Date format in yyyy/mm/dd
Date.prototype.yyyymmdd = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = this.getDate().toString();
        return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // padding
};

var pdfViewedCounter = 0;
var mainPdfViewedCounter = 0;
//// This method is for page SP061001.html and it is ued for when pdf button click then change its text, color and background color.
//// here id for applying css on that element and divflag is for there is one button which occupien whole div to affect that div divflag needs to be 0
function visitedPDF(id, divFlag) {
    if (($(id).css("background-color") != "rgb(244, 244, 244)")) {
        $(id + ' .m1-link-status-text').text('閲覧済');
        $(id + ' .m1-link-status-text').css("color", "#999999");
        $(id + ' .js-m1-link-status-text').css("color", "#999999");
        $(id + ' .js-m1-link-black').css("color", "#333333");
        
        if (divFlag === 0) {
            $(id).css("background-color", "#f4f4f4");
            mainPdfViewedCounter++;
            if (mainPdfViewedCounter==2)
            {
                $('#check1').removeAttr("disabled");
            }
            if ($('#pdfViewedFlag').length !== 0) {
                if ($('#pdfViewedFlag').val() == "true" && id == "#m1pdflink3") {
                        $('#check1').removeAttr("disabled");
                }
            }
        }
        else {
            $(id).css("background-color", "#f4f4f4");
            pdfViewedCounter++;

            if ($('.js-paymentPdf').length !== 0) {
                var counterPdf = 0;
                $('.js-paymentPdf').each(function (index) {
                    counterPdf++;
                });
                if ($('#pdfRead').length !== 0) {
                    if (pdfViewedCounter == counterPdf) {
                        $('#pdfRead').removeAttr("disabled");
                    }
                }                
            }
            //// Following loop is for enabling disabled button after clicking on PDF button.
            if ($('.js-AgreeContinue').length !== 0) {
                $('.js-AgreeContinue').removeClass('rf-button-disabled').addClass('disabled');
            }
        }
    }
}

////  This is for SP061101 js code end here
//// fuction for clicking delete button (SP03 series)
function deleteFund() {
        var result = confirm('選択された銘柄を削除します。\nよろしいですか？');
        if (result) {
            alert('ご指定のファンドを削除しました。');
            document.location = 'SP030101.html';
        }
        else {
            document.location = 'SP030101.html#myModal';
        }
}

//// To redirect to particular location after close modal.
function registerFavoriteFund(location) {
        if (!alert('お気に入りファンドに登録しました。')) {
            document.location = location;
        }
}

//// Used to switch to tab on modal close and then to desired tab
function regNavScreenfromTab(val) {
        if (!alert('お気に入りファンドに登録しました。')) {
            document.location = 'SP020501.html?Tab=' + val;
        }
}

//// function for error alert Not Select Termination Radio Button (SP05 06 02)
function errNotSelectAmountButton() {
        alert('全部解約または一部解約を選択してください。');
}

//// function for error alert Exceed Nine Digit (SP05 06 02)
function errExceedNineDigit() {
        alert('口数は、半角数字９桁以内で入力してください。');
}

//// function for error alert Termination Amount Exceeding Holding Amount (SP05 06 02)
function errTerminationExceedHolding() {
        alert('解約数量がお客様の保有数量を超えています。');
}

//// function for error alert Terminate Twice (SP05 06 02)
function errTerminateTwice() {
        alert('同一約定日に、同一口座内で同一ファンドを複数回解約することは出来ません。');
}

//// function for error alert Termination Exceeding Holding (SP05 06 02)
function errOverlapWithRakuWrap() {
        alert('ご指定のファンドは、楽ラップで重複保有されています。');
}

//// function for error alert incorrect PIN (SP05 06 03, SP05 08 01)
function errIncorectPIN() {
        alert('暗証番号が間違っています。再入力してください。');
}

//// on window click other than modal then close help modal for that storing modal ID currently open and on close we are clearing this var value 
var currentModal;

//// Function to align div in middle
function setModalMiddle(val)
{
    currentModal = val;
    $('body').css('overflow','hidden');
    $('body').addClass('m1-modal-open');
    $('.m1-modal-overlay').show();
    $(val).show();
}

//// Function to align FooterModal or chartTab modal in middle
function setFooterModalMiddle(val) {
        currentModal = val;
        $(val).css('display', 'block');
        $(val).css("opacity", "1");
        $("body").css('overflow', 'hidden');
        $("body").addClass('m1-modal-open');
        $('.m1-modal-overlay').show();
}

//// Reset form SP080201.html
function resetCheckField() {
    $('#searchConditionModal')[0].reset();
        //// Set values to null date field.
        $("input[type=date]").each(function resetDate() {
            this.value = '';
        });
        
        //// For hidding error div
        $('#ErrorDiv').hide();
        //// Set Combobox Default value
        $('#cbInvestment').val("a");
        $('#fundStatus').val("a");
        $('#paymentMethod').val("a");
        $('#reserveDateFrom').val('0');
        $('#reserveDateTo').val('0');
}

//// This method is for setting default values.
function setDefaultDates(val) {
        var today = new Date();
        var yesterday = new Date(today.getTime() - (1 * 24 * 60 * 60 * 1000));
        var last = yesterday;
        switch(val) {
            case '1D':
                last = yesterday;
                break;
            case '1M':
                last.setMonth( yesterday.getMonth( ) - 1 );
                break;
            case '3M':
                last.setMonth( yesterday.getMonth( ) - 3 );
                break;
            case '6M':
                last.setMonth( yesterday.getMonth( ) - 6 );
                break;
            case '12M':
                last.setMonth(yesterday.getMonth() - 12);
                break;
        }
        $('#dtTo1').val(yesterday.yyyymmdd());
        $('#dtFrom1').val(last.yyyymmdd());
        $('#dtTo2').val(yesterday.yyyymmdd());
        $('#dtFrom2').val(last.yyyymmdd());
}

//====================================================================================
//// This method is for showing navigation section on swipe right and swipe left event.
function showNavSections(hideElement1, hideElement2, showElement1, showElement2) {
        $(hideElement1).removeClass('active');
        $(showElement1).addClass('active');
        $(hideElement2).hide();
        $(showElement2).show();
}

//// This methods is for hiding div on button click and both function used to merge two pages in one page.
function redirectToPage2() {
        $('#m1-fund-top-investment-area-ForPage1').hide();
        $('#m1-fund-top-investment-area-ForPage2').show();
}

function redirectToPage1() {
        $('#m1-fund-top-investment-area-ForPage2').hide();
        $('#m1-fund-top-investment-area-ForPage1').show();
}

//// this is function is used for on click of tab switching arrow ex. next tab arrow or prev tab arrow to switch to next tab as tab index start from 0 but in html it is start from 1. 
function showTabs(currentTabIndex, nextTabIndex) {
        var $tabs = $('ul[data-role="nd2tabs"] >li > a');
        $tabs.eq(nextTabIndex - 1).click();
}

//// This method is for showing showing arrow next and previous on all tabs.
function showNextAndPrevArrow(targetTabIndex) {
	    var i = 0;
	    switch (targetTabIndex) {
	        case '#t1':
	            i = 1;
	            break;
	        case '#t2':
	            i = 2;
	            break;
	        case '#t3':
	            i = 3;
	            break;
	        case '#t4':
	            i = 4;
	            break;
	        case '#t5':
	            i = 5;
	            break;
	        case '#t6':
	            i = 6;
	            break;
	        case '#t7':
	            i = 7;
	            break;
	        case '#t8':
	            i = 8;
	            break;
	    }
	    $('#divTab_' + i).show().delay(2000).hide(0);
}

//// For closing modals this method is used
function setClassToDisplayNone(id) {
    currentModal = "";
    $('body').removeClass('m1-modal-open');
	$("body").css('overflow', 'auto');
    $(id).hide();
    $('.m1-modal-overlay').hide();
}

//// This method is for detect whether swipe right event occur or swipe left event.
function changeNavTab(left) {
	    var $tabs = $('ul[data-role="nd2tabs"] >li > a');
	    var len = $tabs.length;
	    var curidx = 0;
	    $tabs.each(function (idx) {
	        if ($(this).closest("li").hasClass('active')) {
	            curidx = idx;
	        }
	    });
	    var nextidx = 0;
	    if (left) {
	        nextidx = (curidx >= len) ? 0 : curidx + 1;
	    } else {
	        nextidx = (curidx <= 0) ? len : curidx - 1;
	    }
	    $tabs.eq(nextidx).click();
}

//// This class remove classes from anchor tag for provided div.
function removeClassFromAnchor(divId_1, divId_2, className) {
	    $(divId_1).find("a:first").removeClass(className);
	    $(divId_2).find("a:first").removeClass(className);
}

//// This class is used for moved to next tab for which tabId provided.
function moveToNextTab(tabId) {
		$(window).scrollTop(0);
	    var $tabs = $('ul[data-role="nd2tabs"] >li > a');
	    $tabs.eq(tabId).click();
}

//// If start day greater than end day then showing error msg.
//// here we used '!=' but if we use '!==' then it won't work as expected as it yield result true.
function DayChecker() {
    var day1 = $('#reserveDateFrom').val();
    var day2 = $('#reserveDateTo').val();
    if (day1 != 0 && day2 != 0) {
        if (parseInt(day1) > parseInt(day2)) {
            $('#ErrorDiv').show();
        }
        else {
            $('#ErrorDiv').hide();
        }
    }
    else
    {
        $('#ErrorDiv').hide();
    }
}

// sticky header menu
$(window).scroll(function () {
	if($('#dvExpandedMenu_1').length !== 0) {
		$('.rf-header-category-menu-inner').hide();
	}
	
    if ($('#sticky-anchor').length !== 0) {
        var window_top = $(window).scrollTop();
        var div_top = $('#sticky-anchor').offset().top;
        if (window_top > div_top) {
            $('#sticky').addClass('stick');
            $('#sticky-anchor').height($('#sticky').outerHeight());
        } else {
            $('#sticky').removeClass('stick');
            $('#sticky-anchor').height(0);
        }
    }
});

//// Method for form submission and prevent if error is occured
function formSubmission() {
    if ($('#ErrorDiv').css("display") == "block") {
        event.preventDefault();
    }
    else {
        document.getElementById('searchConditionModal').submit();
    }
}

//// setting  modal height
function setHeight() {
    windowHeight = $(window).innerHeight();
    $('.js-windowModal').css('min-height', windowHeight);
};

//// on after page show to user show acive menu and active expanded menu if any.
function pageMenuSelector($menu_1, $menu_2, $addLinkMenu) {
    removeClassFromAnchor($menu_1, $menu_2, "rf-header-category-link--active");
    $addLinkMenu.find('a:first').addClass("rf-header-category-link--active");
}
function checkRadioValue() {
	if($('input[name="fundReg020502"]:checked').length == '0') {
		alert('please select atleast one radio button value');
	}
}

//// Prevent user to enter only digit not alphabet
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

//// Make button enable on both checkboxes selected else disable.
function checkPDFIagree() {
    $("#Indicate").attr("disabled", ($("#check1").is(':checked') == true && $("#pdfRead").is(':checked') == true) ? false : true);
}
// new Function for showing current Active Tab & hide InActive Tab for SP020501 page.
function swipeLeftOrRightOnTouchStart() {
	var eventHandlers =  {
		panright: function(){},
		press: function(){},
		panleft:function(){}
	}
	$( "#MainDivForSwipe" ).on("panright press panleft", function(ev) {
		$('.js-showTabOnLeftRight').css('display','block');
		if(ev.type === 'panright') {
			//$('.m1-arrow-back').addClass('is-show-back-transition');
			$('.m1-arrow-back').addClass('m1-arrow-back');
			$('.m1-arrow-next').removeClass('is-show-next');
			$('.m1-arrow-next').removeClass('is-show-next-transition');
		} else if((ev.type === 'panleft')){
			//$('.m1-arrow-next').addClass('is-show-next-transition');
			$('.m1-arrow-next').addClass('m1-arrow-next');
			$('.m1-arrow-back').removeClass('is-show-back');
			$('.m1-arrow-back').removeClass('is-show-back-transition');
		}
	});

}
function swipeLeftOrRightOnTouchEnd(ev) {
	$('.js-showTabOnLeftRight').delay(5000).hide(0)
}
function resetCheckbox() {
	
	$(':checkbox').each(function(i,item){ 
        this.checked = item.defaultChecked; 
	});
}

//// On next tab click show that tab or on slide right and left show that tab.
if($('.js-tab-menu ').length !== 0 && $('#tabsStickyNav').length !== 0) {
	var items = $('.js-tab-menu > li'),
    selectedIndex = 0,
    scroller = $(".numWrap"),
    scrollerWidth = scroller.width();
}
function selectItem() {
    var selected = items.eq(selectedIndex);
	items.removeClass('active');
    selected.addClass('active');
    focus(selected.position().left + selected.width()*0.5);
	showNextAndPrevArrow(selected.find('a:first').attr("href"));
}

function focus(originalLeft) {
    scroll = originalLeft - (scrollerWidth / 2);
    scroller.stop().animate({
        scrollLeft: scroll
    }, 800);
}