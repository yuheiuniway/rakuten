(function($) {

	var loadData = function(data) {
		var listLength = data.length;
		for (var i = 0; i < data.length; i++) {
			var item = data[i];
			var $node = null;
			var html = '<div class="rf-grid rf-grid--1"> \
          <div class="rf-grid-column"> \
            <div class="rf-media rf-mt-15"> \
              <div class="rf-media-left"> \
                <a href="#"> \
                <img class="rf-media-object" src="'+ generateImagePath(item.award) +'" width="60" height="54" alt=""> \
                </a> \
              </div>';
	        html += '<div class="rf-media-body"> \
                <a href="#" class="rf-link-block rf-mv-0 rf-font-bold">'+ item.name +'</a> \
                <p class="rf-mv-0 rf-mini rf-font-bold">'+ item.company +'</p> \
              </div>';
            html += '<ul class="rf-block-list rf-mb-5"> \
                <li class="rf-block"> \
                  <div class="rf-block-inner">手数料</div> \
                  <div class="rf-block-inner rf-align-right rf-font-bold">'+ item.commission +'</div> \
                </li>';
            html += '<li class="rf-block"> \
                  <div class="rf-block-inner">信託報酬<span class="rf-mini rf-font-normal">（税込）</span></div> \
                  <div class="rf-block-inner rf-align-right rf-font-bold">'+ item.fee +' %</div> \
                </li>';
            html += '<li class="rf-block"> \
                  <div class="rf-block-inner">純資産</div> \
                  <div class="rf-block-inner rf-align-right"><strong>'+ item.assets +' 億</strong>円</div> \
                </li>';
            html += '<li class="rf-block"> \
                  <div class="rf-block-inner">トータルリターン(3年)</div> \
                  <div class="rf-block-inner rf-align-right rf-font-bold">'+ item.return +' %</div> \
                </li> \
              </ul>';
            html += '<h4 class="rf-normal rf-font-bold rf-mt-0">ここがポイント!</h4> \
              <p class="rf-mb-20">'+ item.description +'</p> \
            </div> \
          </div> \
        </div>';
        	html += '<!-- Gray button section starts here --> \
        <div class="rf-bg-gainsboro m1-border-bottom-silver rf-pt-10 rf-pb-10"> \
          <div class="rf-grid rf-grid--1"> \
            <div class="rf-grid-column"> \
              <div class="rf-media"> \
                <div class="rf-media-left"> \
                  <img class="rf-media-object m1-image-border" src="'+ item.image +'" width="69" height="96" alt=""> \
                </div>';
            html += '<div class="rf-media-body"> \
                  <a href="#" onclick="'+ item.detail_onclick +'" class="rf-button rf-mt-0" role="button">個別銘柄の詳細へ</a> \
                  <div class="rf-form-button-group-double"> \
                    <div class="rf-form-button-group-inner"> \
                      <a href="#" onclick="'+ item.buy_onclick +'" class="rf-button-positive rf-small rf-pt-5 rf-pb-5" role="button">買い</a> \
                    </div> \
                    <div class="rf-form-button-group-inner"> \
                      <a href="#" onclick="'+ item.establish_onclick +'" class="rf-button-primary rf-small rf-pt-5 rf-pb-5" role="button">積立</a> \
                    </div> \
                  </div> \
                 </div>';
            html += '</div> \
            </div> \
          </div> \
        </div> \
        <!-- Gray button section ends here -->';
	        $node = $(html);
	        $('#'+item.category).append($node);
		}
	};

	var generateImagePath = function(number) {
		number = Number(number);
		var imgPath;
		switch(number) {
			case 1:
				imgPath = '/member/html/smt/images/badge_best.png';
				break;
			case 2:
				imgPath = '/member/html/smt/images/badge_second.png';
				break;
		}
		return imgPath;
	};

	var setErrorMessage = function(area) {
		var $node = $('<p class="rf-pt-10 rf-pb-10 rf-align-center">データの読み込みにエラーがありました。<br>しばらくしてからもう一度お試しください。</p>');
		area.text('');
		area.append($node);
	};

	$.ajax({
		url: '/member/html/smt/json/fund/fund_award.json',
		type: 'GET',
		success: function(data) {
			loadData(data);
		},
		error: function() {
			console.log('error');
			setErrorMessage($('#stocks_dom'));
			setErrorMessage($('#stocks_for'));
			setErrorMessage($('#bonds_dom'));
			setErrorMessage($('#bonds_for'));
			setErrorMessage($('#reit_dom'));
			setErrorMessage($('#reit_for'));
			setErrorMessage($('#balance_var'));
			setErrorMessage($('#balance_fixed'));
		}
	});

})(jQuery);