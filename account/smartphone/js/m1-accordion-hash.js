(function(){
	var hash = document.location.hash.split('#')[1];
	var element = document.getElementById(hash);
	if (element) {
		var topValue = element.offsetTop;
		element.click();
		setTimeout(function() {
		  document.documentElement.scrollTop = document.body.scrollTop = topValue;
		}, 300);
	};
}());