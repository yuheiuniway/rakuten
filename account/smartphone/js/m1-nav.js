(function () {
    //// For changing tab active status this code is used.
	$('.m1-search-result-nav > li > a ').click(function (e) {
	    e.preventDefault();
	    $('.m1-search-result-nav').find("li").find("a.active").removeClass('active');
	    $(this).addClass('active');
	});
})();