(function () {
    //// If PinCode less than or more than 4 digit then button should be disable for exact match 4 digit it should be enabled.
    $('#pin_id').keyup(function () {
        if ($(this).val().length == 4) {
            $('#can_odr').attr('disabled', false);
        } else if ($(this).val().length == 0) {
            $('#can_odr').attr('disabled', true);
        }
        else {
            $('#can_odr').attr('disabled', true);
        }
    });
})();

//// Prevent user to enter only digit not alphabet
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

