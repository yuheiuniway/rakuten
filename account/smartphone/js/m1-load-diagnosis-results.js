(function($) {
	var $text1 = $("#d-text1");
	var $text2 = $("#d-text2");
	var $description = $("#d-description");
	var $bg = $("#d-bg");
	var diagnosis = {};
	var fundId = 0;
	var setErrorMessage = function() {
		var $textNode = $('<p class="rf-pt-10 rf-pb-10">データの読み込みにエラーがありました。<br>しばらくしてからもう一度お試しください。</p>');
		$description.text('');
		$description.append($textNode);
	};
	var convertLineBreaks = function(string) {
		var formattedString = string.replace(/\\n/g, "<br>");
		return formattedString;
	};
	var loadData = function(fundId, diagnosis) {
		var text1 = diagnosis.diagnosis[fundId].text1;
		var text2 = diagnosis.diagnosis[fundId].text2;
		var description = diagnosis.diagnosis[fundId].description;
		var bg = diagnosis.diagnosis[fundId].bg;

		$bg.css({
			'background-image': 'url('+bg+')',
			'background-size': 'cover',
			'background-repeat': 'no-repeat',
			'background-position': '50% 50%'
		})
		$text1.text(text1);
		$text2.html(convertLineBreaks(text2));
		$description.html(convertLineBreaks(description));
	};

	$.ajax({
		url: '/member/smt/json/fund/questions.json',
		type: 'GET',
		success: function(data) {
			diagnosis = data;
			loadData(fundId, diagnosis);
		},
		error: function() {
			setErrorMessage();
		}
	});

})(jQuery);