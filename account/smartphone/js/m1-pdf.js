(function () {
    //// on click of id  changed status of that div and setting color so tha it is identified that user visited.
    $('#m1-anchor-linkpdf').click(function () {
        $('#m1-anchor-linkpdf .m1-read-status').text('������');
        $('#m1-anchor-linkpdf .m1-read-status').css({ "color": "#999999", "font-weight": "bold" });
        $('#m1-anchor-linkpdf .m1-pdf-box').css("background-color", "#f4f4f4");
    });
})();

var pdfViewedCounter = 0;
var mainPdfViewedCounter = 0;
//// This method is for page SP061001.html and it is ued for when pdf button click then change its text, color and background color.
//// here id for applying css on that element and divflag is for there is one button which occupien whole div to affect that div divflag needs to be 0
function visitedPDF(id, divFlag) {
    if (($(id).css("background-color") != "rgb(244, 244, 244)")) {
        $(id + ' .m1-link-status-text').text('������');
        $(id + ' .m1-link-status-text').css("color", "#999999");
        $(id + ' .js-m1-link-status-text').css("color", "#999999");
        $(id + ' .js-m1-link-black').css("color", "#333333");

        if (divFlag === 0) {
            $(id).css("background-color", "#f4f4f4");
            mainPdfViewedCounter++;
            if (mainPdfViewedCounter == 2) {
                $('#check1').removeAttr("disabled");
            }
            if ($('#pdfViewedFlag').length !== 0) {
                if ($('#pdfViewedFlag').val() == "true" && id == "#m1pdflink3") {
                    $('#check1').removeAttr("disabled");
                }
            }
        }
        else {
            $(id).css("background-color", "#f4f4f4");
            pdfViewedCounter++;

            if ($('.js-paymentPdf').length !== 0) {
                var counterPdf = 0;
                $('.js-paymentPdf').each(function (index) {
                    counterPdf++;
                });
                if ($('#pdfRead').length !== 0) {
                    if (pdfViewedCounter == counterPdf) {
                        $('#pdfRead').removeAttr("disabled");
                    }
                }
            }
            //// Following loop is for enabling disabled button after clicking on PDF button.
            if ($('.js-AgreeContinue').length !== 0) {
                $('.js-AgreeContinue').removeClass('rf-button-disabled').addClass('disabled');
            }
        }
    }
}

////Make button enable on both checkboxes selected else disable.
function checkPDFIagree() {
    $("#Indicate").attr("disabled", ($("#check1").is(':checked') == true && $("#pdfRead").is(':checked') == true) ? false : true);
}