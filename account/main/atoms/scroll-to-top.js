/*!
	Atom: Interactions: Scroll to Top
	─────────────────────────────────────────────────────────────────────
	Version: 1.2.4
	Rakuten Styling Foundation: Main CSS Framework
	Author: Chris Gkilitsas | CWD Strategy Section | Rakuten Inc.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ */


// Element’s ID containing the “Scroll to Top”
var rfScroll2TopEl = document.getElementById('rf-js-scroll-top');

if (rfScroll2TopEl) {

	var rfFooterId = rfScroll2TopEl.getAttribute('data-footer-id'),
	    rfFooterEl = document.getElementById(rfFooterId),
	    rfScroll2TopParentEl = rfScroll2TopEl.parentNode,
	    rfScroll2TopClass = 'rf-scroll-top',
	    rfScroll2TopVisY = 200;

}

// Footer Y position in the page
if (rfFooterEl) {
	var rfFooterY = rfFooterEl.offsetTop - window.innerHeight;
} else {
	var rfFooterY = document.body.clientHeight + 1;
}

// Returns the current scrollbar offsets as the X and Y properties of an object
function rfGetScrollOffsets(w) {

	w = w || window;

	if ( w.pageXOffset !== null ) return {
		x: w.pageXOffset,
		y: w.pageYOffset
	};

	var d = w.document;
	if ( document.compatMode == 'CSS1Compat' ) {
		return {
			x: d.documentElement.scrollLeft,
			y: d.documentElement.scrollTop
		};
	}

	return {
		x: d.body.scrollLeft,
		y: d.body.scrollTop
	};

}

// “Scroll to Top” for browsers that does not support CSS property: “position sticky”
function rfScroll2Top() {

	rfScroll2TopParentEl.setAttribute('style', 'position:relative;');

	if ( rfGetScrollOffsets().y < rfScroll2TopVisY ) {
		rfScroll2TopEl.setAttribute('class', rfScroll2TopClass + ' rf-js-scroll-pre');
	} else if ( rfGetScrollOffsets().y >= rfScroll2TopVisY && rfGetScrollOffsets().y < rfFooterY ) {
		rfScroll2TopEl.setAttribute('class', rfScroll2TopClass + ' rf-js-scroll-fixed');
	} else if ( rfGetScrollOffsets().y >= rfFooterY ) {
		rfScroll2TopEl.setAttribute('class', rfScroll2TopClass + ' rf-js-scroll-absolute');
	}

}

// “Scroll to Top” for browsers that supports the CSS property: “position sticky”
function rfScroll2TopSticky() {

	if ( rfGetScrollOffsets().y < rfScroll2TopVisY ) {
		rfScroll2TopEl.setAttribute('class', rfScroll2TopClass + ' rf-js-scroll-pre');
	} else {
		rfScroll2TopEl.setAttribute('class', rfScroll2TopClass + ' rf-js-scroll-sticky');
	}

}

// Modernizr check for support of the CSS position sticky property
if (rfScroll2TopEl) {
	if (typeof Modernizr == 'object') {
		if (!Modernizr.csspositionsticky) {
			window.onscroll = rfScroll2Top;
		} else {
			window.onscroll = rfScroll2TopSticky;
		}
	} else {
		window.onscroll = rfScroll2Top;
	}
}