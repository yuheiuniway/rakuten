/*!
	Organism: Accordion
	─────────────────────────────────────────────────────────────────────
	Version: 1.3.2
	Rakuten Styling Foundation: Main CSS Framework
	Author: Chris Gkilitsas | CWD Strategy Section | Rakuten Inc.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ */


(function () {

	var d = document,
	    rfAccordionsNoJs = d.querySelectorAll('.rf-accordion-no-js'),
	    rfAccordionsHolder = d.querySelectorAll('.rf-accordion--content-holder'),
	    rfAccordionToggles = d.querySelectorAll('.rf-accordion--trigger'),
	    rfSwitchAccordion,
	    touchSupported = 'ontouchstart' in window,
	    pointerSupported = 'pointerdown' in window;

	for ( var i = 0; i < rfAccordionsNoJs.length; i++ ) {
		rfAccordionsNoJs[i].classList.remove('rf-accordion-no-js');
	}

	function rfSetAriaAttr(el, ariaType, newProperty) {
		el.setAttribute(ariaType, newProperty);
	}

	function rfSetAria(title, content, expanded) {
		switch (expanded) {
			case 'true':
				rfSetAriaAttr(title, 'aria-expanded', 'true');
				rfSetAriaAttr(content, 'aria-hidden', 'false');
				break;
			case 'false':
				rfSetAriaAttr(title, 'aria-expanded', 'false');
				rfSetAriaAttr(content, 'aria-hidden', 'true');
				break;
			default:
				break;
		}
	}

	function rfSetClasses(title, content) {
		title.classList.toggle('rf-is-collapsed');
		title.classList.toggle('rf-is-expanded');
		content.classList.toggle('rf-is-collapsed');
		content.classList.toggle('rf-is-expanded');
	}

	function rfSetContentHeight(content, isOpening) {
		if ( isOpening == true ) {
			content.style.height = content.children[0].offsetHeight + 'px';
		} else {
			content.style.height = '';
		}
	}

	function rfSetInitOpenHeight() {
		for ( var i = 0; i < rfAccordionsHolder.length; i++ ) {
			if ( rfAccordionsHolder[i].classList.contains('rf-is-expanded') ) {
				rfSetContentHeight(rfAccordionsHolder[i], true);
			}
		}
	}

	function rfAddAnimation() {
		for ( var i = 0; i < rfAccordionsHolder.length; i++ ) {
			rfAccordionsHolder[i].classList.add('rf-accordion--anim');
		}
	}

	function rfAccordionInit() {
		setTimeout(rfSetInitOpenHeight, 100);
		setTimeout(rfAddAnimation, 200);
	}

	rfSwitchAccordion = function (e) {
		e.preventDefault();
		var thisAccordion = e.target.parentNode.parentNode.parentNode,
		    thisContent = e.target.parentNode.parentNode.children[1],
		    thisTitle = e.target.parentNode.parentNode.children[0].children[0];
		if ( thisAccordion.classList.contains('rf-accordion') ) {
			var allContents = thisAccordion.getElementsByTagName('dd'),
			    allTitles = thisAccordion.querySelectorAll('dt .rf-accordion--trigger');
			for ( var i = 0; i < allContents.length; i++ ) {
				if ( allContents[i].classList.contains('rf-is-expanded') && allContents[i].getAttribute('id') != thisContent.getAttribute('id') ) {
					rfSetAria(allTitles[i], allContents[i], 'false');
					rfSetContentHeight(allContents[i], false);
					rfSetClasses(allTitles[i], allContents[i]);
				}
			}
		}
		if ( e.target.parentNode.classList.contains('rf-accordion--content') ) {
			thisAccordion = e.target.parentNode.parentNode.parentNode.parentNode,
			thisContent = e.target.parentNode.parentNode.parentNode.children[1],
			thisTitle = e.target.parentNode.parentNode.parentNode.children[0].children[0];
		}
		if ( thisContent.classList.contains('rf-is-collapsed') ) {
			rfSetAria(thisTitle, thisContent, 'true');
			rfSetContentHeight(thisContent, true);
		} else {
			rfSetAria(thisTitle, thisContent, 'false');
			rfSetContentHeight(thisContent, false);
		}
		rfSetClasses(thisTitle, thisContent);
	};

	for ( var i = 0, len = rfAccordionToggles.length; i < len; i++ ) {
		rfAccordionToggles[i].addEventListener('click', rfSwitchAccordion, false);
	}

	document.addEventListener('DOMContentLoaded', function () {
		rfAccordionInit();
	});

}());
