/*!
	Organism: Modal
	─────────────────────────────────────────────────────────────────────
	Version: 1.0.0
	Rakuten Styling Foundation: Main CSS Framework
	Author: Amanda Horiuchi | Customer Experience | Rakuten Securities, Inc.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ */


(function () {

	var body = document.body;
	var modals = document.querySelectorAll('.rf-modal');
	var toggles = document.querySelectorAll('[data-toggle="modal"]');

	var init = function() {
		createOverlay();
		setOpenEvents();
		setCloseEvents();
	};

	var createOverlay = function() {
		if (modals.length > 0) {
			var overlayElement = document.createElement('div');
			overlayElement.className = 'rf-modal-overlay';
			body.appendChild(overlayElement);
		}
	};

	var openModal = function(id) {
		body.className += ' rf-modal-open';
		document.getElementById(id).style.display = 'block';
	};

	var closeModal = function(id) {
		document.getElementById(id).style.display = 'none';
		body.className = document.body.className.replace(' rf-modal-open', '');
	};

	var setOpenEvents = function() {
		for (var i = 0, len = toggles.length; i < len; i++) {
			var target = toggles[i].getAttribute('data-target');
			addOpenEvent(toggles[i], target);
		}
	};

	var addOpenEvent = function(el, target) {
		el.addEventListener('click', function(e){
			e.preventDefault();
			openModal(target);
		})
	};

	var setCloseEvents = function() {
		for (var i = 0, len = modals.length; i < len; i++) {
			addCloseEvent(modals[i]);
		}
	};

	var addCloseEvent = function(el) {
		var modal = el;
		var id = modal.id;
		var closeButtons = modal.querySelectorAll('[data-dismiss="modal"]');
		var dialog = modal.querySelectorAll('.rf-modal-dialog')[0];

		var addCloseButtonEvent = function(el, id) {
			var button = el;
			button.addEventListener('click', function(e){
				e.preventDefault();
				closeModal(id);
			});
		};

		for (var i = 0, len = closeButtons.length; i < len; i++) {
			addCloseButtonEvent(closeButtons[i], id);
		}

		dialog.addEventListener('click', function(e){
			e.stopPropagation();
		});

		if (modal.className.search('rf-modal--full') < 0) {
			modal.addEventListener('click', function() {
				closeModal(id);
			});
		}
	};

	init();

}());
