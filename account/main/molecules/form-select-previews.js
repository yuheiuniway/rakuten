/*!
	Molecules: Forms: Select menu with selected value preview
	─────────────────────────────────────────────────────────────────────
	Version: 1.1.1
	Rakuten Styling Foundation: Main CSS Framework
	Author: Chris Gkilitsas | CWD Strategy Section | Rakuten Inc.
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ */


(function () {

	var d = document,
	    output = '.rf-js-select-output',
	    outputEls = d.querySelectorAll(output),
	    selectMenu = '[class*=\'rf-select\'] > select',
	    selectMenus = d.querySelectorAll(selectMenu),
	    selectMenuGroups = d.querySelectorAll('.rf-js-select-group');

	setSelectOutput = function (el, label, value, txt) {

		var labelEl = el.querySelector('.rf-js-select-output--label'),
		    valueEl = el.querySelector('.rf-js-select-output--value');

		if ( labelEl && valueEl ) {
			if ( value !== '' ) {
				labelEl.innerHTML = label;
				valueEl.innerHTML = txt;
			} else {
				labelEl.innerHTML = valueEl.innerHTML = '';
			}
		}

	};

	if ( outputEls.length !== 0 ) {

		for ( i = 0; i < selectMenus.length; i++ ) {
			selectMenus[i].addEventListener('change', (function (i, e) {

				var menu = selectMenus[i].options[selectMenus[i].selectedIndex],
				    label = selectMenus[i].getAttribute('data-label'),
				    parentEl = selectMenus[i].parentNode.parentNode,
				    outputEl;

				if ( selectMenuGroups.length !== 0 && parentEl.classList.contains('rf-js-select-group') ) {

					var groupMenus = parentEl.querySelectorAll(selectMenu),
					    groupText = '',
					    groupValue = '';

					for ( j = 0; j < groupMenus.length; j++ ) {

						var groupMenu = groupMenus[j].options[groupMenus[j].selectedIndex];

						if ( groupMenu.value !== '' ) {
							groupValue += groupMenu.value;
						}

						if ( groupMenu.value ) {
							groupText += groupMenu.text;
							if ( (j+1) !== groupMenus.length ) {
								groupText += ' ~ ';
							}
						}

					}

					outputEl = parentEl.parentNode.querySelector(output);
					if ( outputEl !== null ) {
						setSelectOutput(outputEl, label, groupValue, groupText);
					}
				} else {
					outputEl = parentEl.querySelector(output);
					if ( outputEl !== null ) {
						setSelectOutput(outputEl, label, menu.value, menu.text);
					}
				}

			}).bind(this, i), false);
		}

	}


}());
