/*!
	Styling Foundation: Developer view toggle
	─────────────────────────────────────────────────────────────────────
	Version: 1.1.1
	Rakuten Styling Foundation: Main CSS Framework
	Author: Amanda Horiuchi | Customer Experience | Rakuten Securities
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ */

(function () {
	var $body = $("body");
	var $toggle = $("#developerToggle");

	function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
    }
    return "";
	}

	if (getCookie("sfmode").length > 0) {
		$body.addClass("developer");
		$toggle.text("Exit developer mode");
	}

	$toggle.click(function(e){
		if ($body.hasClass("developer")) {
			$body.removeClass("developer");
			$toggle.text("Enter developer mode");
			setCookie("sfmode", "dev", -10);
		} else {
			$body.addClass("developer");
			$toggle.text("Exit developer mode");
			setCookie("sfmode", "dev", 100);
		}
		e.preventDefault();
	});
}());